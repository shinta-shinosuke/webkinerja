<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Opd extends CI_Controller {

    protected $tblogin    = 'vlogin';
    protected $tblibur    = 'libur';
    protected $tbpns    = 'tbl_pns';
    protected $tbeselon = 'ref_eselon';
    protected $tbgol    = 'ref_golongan';
    protected $tbskp    = 'skp_tahunan';
    protected $tbquant  = 'kuantitas';
    protected $tbkskp   = 'lap_kinerja_skp';
    protected $tbkprod  = 'lap_kinerja_produktivitas';
    protected $tbacc    = 'persetujuan_atasan';
    protected $tbnote    = 'penilaian_perilaku';

	function __construct(){
        parent::__construct();
        $this->load->library(array('session'));
        $this->load->model(array('M_opd'));
        $cek = $this->session->userdata('userkodeadmin');
        if( $cek=='3' || $cek=='')
        {
            echo'<script>alert("Anda tidak diperkenankan mengakses halaman ini. Hubungi Administrator !");window.location.href = "'.base_url('login').'";</script>';       
        }
    }

	public function index()
	{
        $this->template->opd('template', 'opd');
    }

    public function akun()
    {
        $data['title']  = 'Update Password';
        $data['error']  = '';
        $data['nip'] = $this->session->userdata('userid');
        $data['password'] = '';
        $data['passwordbaru'] = '';
        $data['repasswordbaru'] = '';
        $this->template->opd('template', 'akun', $data);
    }

    public function saveakun()
    {
        $nip = $this->input->post('nip');
        $password = $this->input->post('password');
        $passwordbaru = $this->input->post('passwordbaru');
        $repasswordbaru = $this->input->post('repasswordbaru');

        if($passwordbaru!=$repasswordbaru)
        {
            $data['title']  = 'Update Password';
            $data['error']  = 'Password Baru Tidak Sama';
            $data['nip'] = $this->input->post('nip');
            $data['password'] = $this->input->post('password');
            $data['passwordbaru'] = $this->input->post('passwordbaru');
            $data['repasswordbaru'] = $this->input->post('repasswordbaru');
            $this->template->opd('template', 'akun', $data);
        }else{
            $cek = $this->M_opd->cekPasswd($nip);
            foreach ($cek->result_array() as $v) {
                $passwd = $v['repassword'];
            }

            if($password!=$passwd)
            {
                $data['title']  = 'Update Password';
                $data['error']  = 'Password Lama Tidak Sesuai';
                $data['nip'] = $this->input->post('nip');
                $data['password'] = $this->input->post('password');
                $data['passwordbaru'] = $this->input->post('passwordbaru');
                $data['repasswordbaru'] = $this->input->post('repasswordbaru');
                $this->template->opd('template', 'akun', $data);
            }else{
                $this->M_opd->savePasswd($nip,$passwordbaru);
            }
        }
    }

    public function skp()
    {
        $data['pns'] = $this->M_opd->getAll();
        $data['bulan'] = date('m');
        $data['tahun'] = date('Y');
        $this->template->opd('template', 'skp', $data);
    }

    public function addskp()
    {
        $data['pns']        = $this->M_opd->getStaffOpd();
        $data['kuantitas']  = $this->M_opd->getQuant();
        $data['tahun']      = date('Y');
        $data['title']      = 'Input Data SKP Tahunan';
        $data['id']         = '';
        $data['nip']        = '';
        $data['kegiatan']   = '';
        $data['targetkuantitas'] = '';
        $data['satuankuantitas'] = '';
        $data['targetpenyelesaian'] = '';
        $this->template->opd('template', 'addskp', $data);   
    }

    public function editskp($id)
    {
        $data['title']      = 'Edit Data SKP Tahunan';
        $data['pns']        = $this->M_opd->getStaffOpd();
        $data['kuantitas']  = $this->M_opd->getQuant();
        $ambil              = $this->M_opd->getById($id);
        foreach ($ambil->result_array() as $key => $v) {
            $data['id']        = $v['id_skp'];
            $data['tahun']      = $v['tahun'];
            $data['nip']        = $v['nip'];
            $data['kegiatan']   = $v['kegiatan'];
            $data['targetkuantitas'] = $v['target_kuantitas'];
            $data['satuankuantitas'] = $v['satuan_kuantitas'];
            $data['targetpenyelesaian'] = $v['target_selesai'];
        }
        $this->template->opd('template', 'addskp', $data);   
    }

    public function saveskp()
    {
        $url = $this->input->post('url');
        $id = $this->input->post('id');
        $nip = $this->input->post('nip');
        $tahun = $this->input->post('tahun');
        $kegiatan = $this->input->post('kegiatan');
        $targetkuantitas = $this->input->post('targetkuantitas');
        $satuankuantitas = $this->input->post('satuankuantitas');
        $targetpenyelesaian = $this->input->post('targetpenyelesaian');
        //echo $id;

        if($id>0){
            $upd = $this->M_opd->editSkp($url, $id, $nip, $tahun, $kegiatan, $targetkuantitas, $satuankuantitas, $targetpenyelesaian);
        }else{
            $ins = $this->M_opd->saveSkp($url, $id, $nip, $tahun, $kegiatan, $targetkuantitas, $satuankuantitas, $targetpenyelesaian);
        }
    }

    public function kskp()
    {
        $this->template->opd('template', 'kskp');
    }

    public function addkskp()
    {
        $data['title']   = 'Input Data Laporan Kinerja SKP';
        $data['tanggal'] = date('d-m-Y H:i:s');
        $data['pns']     = $this->M_opd->getStaffOpd();
        $data['id']      = '';
        $data['error']   = '';
        $data['getskp']  = '';
        $data['nip']     = '';
        $data['skp']     = '';
        $data['kegiatan']= '-';
        $data['kuantitas']  = '';
        $data['satuan']  = '';
        $data['status']  = '-';
        $data['sesuai']  = '-';
        $this->template->opd('template', 'addkskp', $data);      
    }

    public function editkskp($id)
    {
        $data['title']      = 'Edit Data Laporan Kinerja SKP';
        $data['pns']        = $this->M_opd->getStaffOpd();
        $ambil              = $this->M_opd->getKskpById($id);
        foreach ($ambil->result_array() as $key => $v) {
            $data['id']         = $v['id_lap_skp'];
            $waktu = $v['tanggal'];
            $tgl = substr($waktu, 8,2);
            $bln = substr($waktu, 5,2);
            $thn = substr($waktu, 0,4);
            $jam = substr($waktu, -9);
            $time = $tgl.'-'.$bln.'-'.$thn.$jam;
            $data['tanggal']    = $time;
            $data['nip']        = $v['nip'];
            $data['skp']        = $v['id_skp'];
            $data['numsatuan']  = $v['numsatuan'];
            $data['satuan']     = $v['satuan'];
            $data['kuantitas']  = $v['tercapai'];
            $data['kegiatan']   = $v['pekerjaan'];
            $data['status']     = $v['status'];
            $data['sesuai']     = $v['sesuai'];
            $nip = $v['nip'];
        }
        $data['getskp']              = $this->M_opd->getskptahunan($nip);
        $data['error'] ='';
        $this->template->opd('template', 'addkskp', $data);   
    }

    public function myformAjax($id) { 
        $cek = $this->M_opd->cek_lap_kinerja($id);
        if($cek>0){
            $result = $this->db->where("nip",$id)->get("v_satuan")->result();   
        }else{
           $result = $this->db->where("nip",$id)->get("v_satuan")->result();
        }
           echo json_encode($result);
    }

    public function myeditformAjax($id) { 
        $result = $this->db->where("id_skp",$id)->get("v_satuan")->result();   
        echo json_encode($result);
    }

    public function savekskp()
    {
        $url = $this->input->post('url');
        $id = $this->input->post('id');
        $tanggal = $this->input->post('tanggal');
        $nip = $this->input->post('nip');
        $id_skp = $this->input->post('skp');
        $kegiatan = $this->input->post('kegiatan');
        $kuantitas = $this->input->post('kuantitas');
        $satuan = $this->input->post('numsatuan');
        $status = $this->input->post('status');
        $sesuai = $this->input->post('sesuai');
        //echo $id;

        $tgl = substr($tanggal, 0,2);
        $bln = substr($tanggal, 3,2);
        $thn = substr($tanggal, 6,4);
        $ambiltgl = $thn.'-'.$bln.'-'.$tgl;
        $cek = $this->M_opd->ceklibur($ambiltgl);
        if($cek->num_rows()>0)
        {
            $data['title']      = 'Edit Data Laporan Kinerja SKP';
            $data['pns']        = $this->M_opd->getAll();
                $data['id']         = $this->input->post('id');
                $data['tanggal']    = $this->input->post('tanggal');
                $data['nip']        = $this->input->post('nip');
                $data['skp']        = $this->input->post('skp');
                $data['numsatuan']  = $this->input->post('numsatuan');
                $data['satuan']     = $this->input->post('satuan');
                $data['kuantitas']  = $this->input->post('kuantitas');
                $data['kegiatan']   = $this->input->post('kegiatan');
                $data['status']     = $this->input->post('status');
                $data['sesuai']     = $this->input->post('sesuai');
                $nip = $this->input->post('nip');
                $data['getskp']              = $this->M_opd->getskptahunan($nip);
                foreach ($cek->result_array() as $key => $c) {
                    $data['error'] =$c['keterangan'];
                }
            $this->template->opd('template', 'addkskp', $data);   
        }else{
            if($id>0){
                $upd = $this->M_opd->edit_lap_skp($url, $id, $tanggal, $nip, $id_skp, $kegiatan, $kuantitas, $satuan, $status, $sesuai);
            }else{
                $ins = $this->M_opd->save_lap_skp($url, $id, $tanggal, $nip, $id_skp, $kegiatan, $kuantitas, $satuan, $status, $sesuai);
            }
        }
    }

    public function kprod()
    {
        $this->template->opd('template', 'kprod');
    }

    public function addkprod()
    {
        $data['title']   = 'Input Data Laporan Kinerja Produktivitas';
        $data['tanggal'] = date('d-m-Y H:i:s');
        $data['pns']     = $this->M_opd->getStaffOpd();
        $data['id']      = '';
        $data['error']   = '';
        $data['getskp']  = '';
        $data['nip']     = '';
        $data['kegiatan']= '-';
        $data['kuantitas']  = '';
        $data['satuankuantitas']  = '';
        $data['satkuantitas']  = $this->M_opd->getQuant();
        $data['status']  = '-';
        $data['sesuai']  = '-';
        $this->template->opd('template', 'addkprod', $data);      
    }

    public function editkprod($id)
    {
        $data['title']      = 'Edit Data Laporan Kinerja Produktivitas';
        $data['pns']        = $this->M_opd->getStaffOpd();
        $ambil              = $this->M_opd->getKprodById($id);
        foreach ($ambil->result_array() as $key => $v) {
            $data['id']         = $v['id_lap_prod'];
            $waktu = $v['tanggal'];
            $tgl = substr($waktu, 8,2);
            $bln = substr($waktu, 5,2);
            $thn = substr($waktu, 0,4);
            $jam = substr($waktu, -9);
            $time = $tgl.'-'.$bln.'-'.$thn.$jam;
            $data['tanggal']    = $time;
            $data['nip']        = $v['nip'];
            $data['kegiatan']   = $v['kegiatan'];
            $data['kuantitas']  = $v['qty'];
            $data['satuankuantitas']     = $v['satuan'];
            $data['satkuantitas']  = $this->M_opd->getQuant();
            $data['status']     = $v['status'];
            $nip = $v['nip'];
        }
        $data['error'] ='';
        $this->template->opd('template', 'addkprod', $data);   
    }

    public function savekprod()
    {
        $url = $this->input->post('url');
        $id = $this->input->post('id');
        $tanggal = $this->input->post('tanggal');
        $nip = $this->input->post('nip');
        $kegiatan = $this->input->post('kegiatan');
        $kuantitas = $this->input->post('kuantitas');
        $satuan = $this->input->post('satuankuantitas');
        $status = $this->input->post('status');

        $tgl = substr($tanggal, 0,2);
        $bln = substr($tanggal, 3,2);
        $thn = substr($tanggal, 6,4);
        $ambiltgl = $thn.'-'.$bln.'-'.$tgl;
        $cek = $this->M_opd->ceklibur($ambiltgl);
        if($cek->num_rows()>0)
        {
            $data['title']      = 'Edit Data Laporan Kinerja Produktivitas';
            $data['pns']        = $this->M_opd->getAll();
                $data['id']         = $this->input->post('id');
                $data['tanggal']    = $this->input->post('tanggal');
                $data['nip']        = $this->input->post('nip');
                $data['satkuantitas']  = $this->M_opd->getQuant();
                $data['satuankuantitas']     = $this->input->post('satuankuantitas');
                $data['kuantitas']  = $this->input->post('kuantitas');
                $data['kegiatan']   = $this->input->post('kegiatan');
                $data['status']     = $this->input->post('status');
                $nip = $this->input->post('nip');
                foreach ($cek->result_array() as $key => $c) {
                    $data['error'] =$c['keterangan'];
                }
            $this->template->opd('template', 'addkprod', $data);   
        }else{
            if($id>0){
                $upd = $this->M_opd->edit_lap_kprod($url, $id, $tanggal, $nip, $kegiatan, $kuantitas, $satuan, $status);
            }else{
                $ins = $this->M_opd->save_lap_kprod($url, $id, $tanggal, $nip, $kegiatan, $kuantitas, $satuan, $status);
            }
        }
    }
    
    public function acc()
    {
        $this->template->opd('template', 'acc');
    }

    public function setting()
    {
        $data['bulan'] = date('m');
        $data['tahun'] = date('Y');
        $this->template->opd('template', 'setting', $data);
    }

    public function addsetting()
    {
        $data['title']   = 'Input Data Hari Libur Kerja';
        $data['id']      = '';
        $data['keterangan']      = '';
        $data['tanggal'] = date('d-m-Y');
        $this->template->opd('template', 'addsetting', $data);
    }

    public function editsetting($id)
    {
        $get = $this->M_opd->getlibur($id);
        foreach ($get as $key => $value) {
            $data['id']         = $value['idlibur'];
            $data['keterangan'] = $value['keterangan'];
            $tanggl = $value['tanggal'];
            $tgl = substr($tanggl, -2);
            $bln = substr($tanggl, 5, 2);
            $thn = substr($tanggl, 0, 4);
            $data['tanggal']    = $tgl.'-'.$bln.'-'.$thn;
        }
        $this->template->opd('template', 'addsetting', $data);
    }

    public function savesetting()
    {
        $id      = $this->input->post('id');
        $keterangan      = $this->input->post('keterangan');
        $tanggal         = $this->input->post('tanggal');
        
        if($id>0){
            $this->M_opd->updsetting($id, $keterangan, $tanggal);
        }else{
            $this->M_opd->inssetting($id, $keterangan, $tanggal);
        }

    }

    public function ajax_libur()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            
            //panggil dulu library datatablesnya
            $this->load->library('SSP');
            
            //atur nama tablenya disini
            $table = $this->tblibur;

            // Table's primary key
            $primaryKey = 'idlibur';

            $columns = array(
                array(
                    'db' => 'tanggal',   'dt' => 'tanggal', 
                    'formatter'=> function($d, $row){
                        $tmhn = $row['tanggal'];
                        $tgl  = substr($tmhn, -2);
                        $bln  = substr($tmhn, 5, 2);
                        $thn  = substr($tmhn, 0, 4);
                        return $tgl.'-'.$bln.'-'.$thn;
                    },'field' => 'tanggal'),
                array('db' => 'keterangan',    'dt' => 'keterangan',          'field' => 'keterangan'),
                array( 
                    'db' => 'idlibur', 'dt' => 'aksi',
                    'formatter' => function( $d, $row) {
                            return '<a class="btn btn-sm btn-primary" href="javascript:void(0)" onclick="edit_libur('."'".$d."','".$row['idlibur']."'".')"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;Edit</a>';
                        }, 'field' => 'idlibur'),
            );

            // SQL server connection information
            $sql_details = array(
                'user' => $this->config->item('user'),
                'pass' => $this->config->item('pass'),
                'db'   => $this->config->item('db'),
                'host' => $this->config->item('host')
            );

            $joinQuery = "FROM ".$this->tblibur; 
            $extraWhere= "keterangan is not null";

            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
            );
        }
    }

    public function ajax_skp()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            
            //panggil dulu library datatablesnya
            $this->load->library('SSP');
            
            //atur nama tablenya disini
            $table = $this->tbskp;

            // Table's primary key
            $primaryKey = 'id_skp';

            $columns = array(

                array('db' => 's.nip',          'dt' => 'nip',          'field' => 'nip'),
                array('db' => 's.tahun',        'dt' => 'tahun',        'field' => 'tahun'),
                array('db' => 'p.nama',         'dt' => 'nama',        'field' => 'nama'),
                array('db' => 'l.opd',         'dt' => 'opd',        'field' => 'opd'),
                array('db' => 's.kegiatan',     'dt' => 'kegiatan',      'field' => 'kegiatan'),
                array('db' => 'q.id',     'dt' => 'id_kuantitas',      'field' => 'id'),
                array('db' => 'q.kuantitas',     'dt' => 's_kuantitas',      'field' => 'kuantitas'),
                array('db' => 's.satuan_kuantitas','dt' => 'satuan_kuantitas', 'field' => 'satuan_kuantitas'),
                array('db' => 's.target_kuantitas','dt' => 't_kuantitas', 'field' => 'target_kuantitas'),
                array(
                    'db' => 's.target_selesai',  'dt' => 't_selesai', 
                    'formatter' => function( $d, $row) {
                                        return $row['target_selesai'].' Bulan';
                                    },'field' => 'target_selesai'),
                array( 
                    'db' => 's.id_skp', 'dt' => 'aksi',
                    'formatter' => function( $d, $row) {
                            //return '<a class="btn btn-sm btn-primary" href="javascript:void(0)" onclick="edit_pns('."'".$d."','".$row['id_skp']."'".')"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;Edit</a>';
                        return '<a class="btn btn-sm btn-primary" href="'.base_url().'opd/editskp/'.$row['id_skp'].'"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;Edit</a>';
                        }, 'field' => 'id_skp'),
            );

            // SQL server connection information
            $sql_details = array(
                'user' => $this->config->item('user'),
                'pass' => $this->config->item('pass'),
                'db'   => $this->config->item('db'),
                'host' => $this->config->item('host')
            );

            $joinQuery = "FROM ".$this->tbskp." s JOIN ".$this->tbpns." p JOIN ".$this->tbquant." q JOIN ".$this->tblogin." l"; 
            $extraWhere= "s.nip=p.nip AND s.satuan_kuantitas=q.id AND p.nip=l.nip AND l.opd='".$this->session->userdata('useropd')."'";

            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
            );
        }
    }

    public function ajax_kskp()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            
            //panggil dulu library datatablesnya
            $this->load->library('SSP');
            
            //atur nama tablenya disini
            $table = $this->tbskp;

            // Table's primary key
            $primaryKey = 'id_skp';

            $columns = array(

                array('db' => 'a.nip',          'dt' => 'nip',          'field' => 'nip'),
                array('db' => 'date_format(c.tanggal, "%d-%m-%Y") as tanggal',    'dt' => 'tanggal',      'field' => 'tanggal'),
                array('db' => 'TIME(c.tanggal) as waktu',      'dt' => 'waktu',      'field' => 'waktu'),
                array('db' => 'a.kegiatan as skptahunan',     'dt' => 'skptahunan',     'field' => 'skptahunan'),
                array('db' => 'c.kegiatan',     'dt' => 'kegiatan',     'field' => 'kegiatan'),
                array('db' => 'a.target_kuantitas',  'dt' => 'target',  'field' => 'target_kuantitas'),
                array('db' => 'k.id',           'dt' => 'numsatuan',    'field' => 'id'),
                array('db' => 'l.opd',         'dt' => 'opd',        'field' => 'opd'),
                array('db' => 'k.kuantitas as satuan',    'dt' => 'satuan',       'field' => 'satuan'),
                array('db' => 'c.kuantitas',    'dt' => 'tercapai',     'field' => 'kuantitas'),
                array('db' => 'a.tahun',        'dt' => 'tahun',   'field' => 'tahun'),
                array('db' => 'c.id_skp_tahunan','dt' => 'id_skp_tahunan','field' => 'id_skp_tahunan'),
                array('db' => 'c.sesuai',       'dt' => 'sesuai',        'field' => 'sesuai'),
                array('db' => 'c.status',       'dt' => 'status',        'field' => 'status'),
                array( 
                    'db' => 'c.id_lap_skp', 'dt' => 'aksi',
                    'formatter' => function( $d, $row) {
                            //return '<a class="btn btn-sm btn-primary" href="javascript:void(0)" onclick="edit_pns('."'".$d."','".$row['id_lap_skp']."'".')"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;Edit</a>';
                        return '<a class="btn btn-sm btn-primary" href="'.base_url().'opd/editkskp/'.$row['id_lap_skp'].'"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;Edit</a>';
                        }, 'field' => 'id_lap_skp'),
            );

            // SQL server connection information
            $sql_details = array(
                'user' => $this->config->item('user'),
                'pass' => $this->config->item('pass'),
                'db'   => $this->config->item('db'),
                'host' => $this->config->item('host')
            );

            $joinQuery = "FROM ".$this->tbskp." AS a, ".$this->tbquant." AS k, ".$this->tbkskp." AS c JOIN ".$this->tblogin." l"; 
            $extraWhere= "a.satuan_kuantitas=k.id AND a.id_skp=c.id_skp_tahunan AND a.nip=l.nip AND l.opd='".$this->session->userdata('useropd')."'";


            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
            );
        }
    }

    public function ajax_kprod()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            
            //panggil dulu library datatablesnya
            $this->load->library('SSP');
            
            //atur nama tablenya disini
            $table = $this->tbkprod;

            // Table's primary key
            $primaryKey = 'id_lap_prod';

            $columns = array(

                array('db' => 'p.nip',          'dt' => 'nip',          'field' => 'nip'),
                array('db' => 'p.tanggal',      'dt' => 'tanggal',      'field' => 'tanggal'),
                array('db' => 'p.kegiatan',     'dt' => 'kegiatan',     'field' => 'kegiatan'),
                array('db' => 'p.kuantitas',    'dt' => 'kuantitas',    'field' => 'kuantitas'),
                array('db' => 'p.satuan',       'dt' => 'satuankuantitas',       'field' => 'satuan'),
                array('db' => 'l.opd',         'dt' => 'opd',        'field' => 'opd'),
                array('db' => 'k.id',           'dt' => 'numsatuan',    'field' => 'id'),
                array('db' => 'k.kuantitas as satkuantitas',    'dt' => 'satkuantitas',       'field' => 'satkuantitas'),
                array('db' => 'p.status',       'dt' => 'status',       'field' => 'status'),
                array( 
                    'db' => 'id_lap_prod', 'dt' => 'aksi',
                    'formatter' => function( $d, $row) {
                            return '<a class="btn btn-sm btn-primary" href="'.base_url().'opd/editkprod/'.$row['id_lap_prod'].'"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;Edit</a>';
                        }, 'field' => 'id_lap_prod'),
            );

            // SQL server connection information
            $sql_details = array(
                'user' => $this->config->item('user'),
                'pass' => $this->config->item('pass'),
                'db'   => $this->config->item('db'),
                'host' => $this->config->item('host')
            );

            $joinQuery = "FROM ".$this->tbkprod." AS p, ".$this->tbquant." AS k JOIN ".$this->tblogin." l"; 
            $extraWhere= "p.satuan=k.id  AND p.nip=l.nip AND l.opd='".$this->session->userdata('useropd')."'";


            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
            );
        }
    }

    public function ajax_acc()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            
            //panggil dulu library datatablesnya
            $this->load->library('SSP');
            
            //atur nama tablenya disini
            $table = $this->tbacc;

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(

                array('db' => 'p.id as id',          'dt' => 'id',          'field' => 'id'),
                array('db' => 'l.id as loginid',          'dt' => 'loginid',          'field' => 'loginid'),
                array('db' => 'p.nip',          'dt' => 'nip',          'field' => 'nip'),
                array('db' => 'p.nip_atasan',   'dt' => 'nip_atasan',   'field' => 'nip_atasan'),
                array('db' => 'p.blth',         'dt' => 'blth',      'field' => 'blth'),
                array('db' => 'p.status',       'dt' => 'status',       'field' => 'status'),
                array('db' => 'p.tgl_persetujuan',     'dt' => 'tgl_persetujuan',     'field' => 'tgl_persetujuan'),
                array('db' => 'p.jml_kegiatan',    'dt' => 'jml_kegiatan',    'field' => 'jml_kegiatan'),
                array('db' => 'p.sumber_data',       'dt' => 'sumber_data',       'field' => 'sumber_data'),
                array('db' => 'l.opd',         'dt' => 'opd',        'field' => 'opd'),
                
                array( 
                    'db' => 'p.id', 'dt' => 'aksi',
                    'formatter' => function( $d, $row) {
                            return '<a class="btn btn-sm btn-primary" href="javascript:void(0)" onclick="edit_pns('."'".$d."','".$row['id']."'".')"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;Edit</a>';
                        }, 'field' => 'id'),
            );

            // SQL server connection information
            $sql_details = array(
                'user' => $this->config->item('user'),
                'pass' => $this->config->item('pass'),
                'db'   => $this->config->item('db'),
                'host' => $this->config->item('host')
            );

            $joinQuery = "FROM ".$this->tbacc." AS p JOIN ".$this->tblogin." l"; 
            $extraWhere= "p.nip=l.nip AND l.opd='".$this->session->userdata('useropd')."'";


            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
            );
        }
    }

    public function note()
    {
        $this->template->opd('template', 'catatan');
    }

    public function ajax_note()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            
            //panggil dulu library datatablesnya
            $this->load->library('SSP');
            
            //atur nama tablenya disini
            $table = $this->tbnote;

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(

                array('db' => 'p.nip',          'dt' => 'nip',          'field' => 'nip'),
                array('db' => 'a.nama',         'dt' => 'nama',          'field' => 'nama'),
                array('db' => 'p.nip_atasan',   'dt' => 'nip_atasan',   'field' => 'nip_atasan'),
                array('db' => 'p.blth',         'dt' => 'blth',      'field' => 'blth'),
                array('db' => 'l.opd',         'dt' => 'opd',        'field' => 'opd'),
                array('db' => 'p.tgl_pembuatan',     'dt' => 'tgl_pembuatan',     'field' => 'tgl_pembuatan'),
                array('db' => 'p.tgl_edit',    'dt' => 'tgl_edit',    'field' => 'tgl_edit'),                
                array( 
                    'db' => 'p.id', 'dt' => 'aksi',
                    'formatter' => function( $d, $row) {
                            return '
                                    <a class="btn btn-sm btn-success" href="javascript:void(0)" onclick="detail_note('."'".$d."','".$row['id']."'".')"><i class="glyphicon glyphicon-eye-open"></i></a>
                                    <a class="btn btn-sm btn-warning" href="'.base_url().'opd/printnote/'.$row['id'].'" target="_blank"><i class="glyphicon glyphicon-print"></i></a>';
                        }, 'field' => 'id'),
            );

            // SQL server connection information
            $sql_details = array(
                'user' => $this->config->item('user'),
                'pass' => $this->config->item('pass'),
                'db'   => $this->config->item('db'),
                'host' => $this->config->item('host')
            );

            $joinQuery = "FROM ".$this->tbnote." AS p, ".$this->tbpns." AS a JOIN ".$this->tblogin." l"; 
            $extraWhere= "p.nip=a.nip  AND a.nip=l.nip AND l.opd='".$this->session->userdata('useropd')."'";


            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
            );
        }
    }

    public function ajax_detailnote($id)
    {
        $data = $this->M_opd->ajax_getNote($id);
        echo json_encode($data);
    }

    public function printnote($id)
    {

        $data['data'] = $this->M_opd->getNote($id);
        $this->load->view('cetakpenilaian',$data);
        $html = $this->output->get_output();
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("penilaian_".$id.".pdf",array('Attachment'=>0));
          
    }

    public function ajax_pnsnote()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            
            //panggil dulu library datatablesnya
            $this->load->library('SSP');
            
            //atur nama tablenya disini
            $table = $this->tbnote;

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(

                array('db' => 'p.nip',          'dt' => 'nip',          'field' => 'nip'),
                array('db' => 'a.nama',         'dt' => 'nama',          'field' => 'nama'),
                array('db' => 'p.nip_atasan',   'dt' => 'nip_atasan',   'field' => 'nip_atasan'),
                array('db' => 'p.blth',         'dt' => 'blth',      'field' => 'blth'),
                array('db' => 'p.tgl_pembuatan',     'dt' => 'tgl_pembuatan',     'field' => 'tgl_pembuatan'),
                array('db' => 'p.tgl_edit',    'dt' => 'tgl_edit',    'field' => 'tgl_edit'),                
                array( 
                    'db' => 'p.id', 'dt' => 'aksi',
                    'formatter' => function( $d, $row) {
                            return '
                                    <a class="btn btn-sm btn-success" href="javascript:void(0)" onclick="detail_note('."'".$d."','".$row['id']."'".')"><i class="glyphicon glyphicon-eye-open"></i></a>
                                    <a class="btn btn-sm btn-warning" href="'.base_url().'opd/printnote/'.$row['id'].'" target="_blank"><i class="glyphicon glyphicon-print"></i></a>';
                        }, 'field' => 'id'),
            );

            // SQL server connection information
            $sql_details = array(
                'user' => $this->config->item('user'),
                'pass' => $this->config->item('pass'),
                'db'   => $this->config->item('db'),
                'host' => $this->config->item('host')
            );

            $joinQuery = "FROM ".$this->tbnote." AS p, ".$this->tbpns." AS a"; 
            $extraWhere= "p.nip=a.nip AND p.nip='".$this->session->userdata('userid')."'";


            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
            );
        }
    }
}