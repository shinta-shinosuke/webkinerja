<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    protected $tblibur  = 'libur';
    protected $tbpns    = 'tbl_pns';
    protected $tbeselon = 'ref_eselon';
    protected $tbgol    = 'ref_golongan';
    protected $tbskp    = 'skp_tahunan';
    protected $tbquant  = 'kuantitas';
    protected $tbkskp   = 'lap_kinerja_skp';
    protected $tbkprod  = 'lap_kinerja_produktivitas';
    protected $tbacc    = 'persetujuan_atasan';
    protected $tbnote   = 'penilaian_perilaku';
    protected $tbpnsxt  = 'tbl_pns_ext';
    protected $tbloker  = 'tab_loker';

	function __construct(){
        parent::__construct();
        $this->load->library(array('session','Myauth'));
        $this->load->model(array('M_opd'));
        $cek = $this->session->userdata('userkodeadmin');
        if( $cek!='1')
        {
            echo'<script>alert("Anda tidak diperkenankan mengakses halaman ini. Hubungi Administrator !");window.location.href = "'.base_url('login').'";</script>';       
        }
    }

	public function index()
	{
        $this->template->opd('template', 'admin_dash');
    }

    public function atasan()
    {
        $this->template->opd('template', 'atasan');
    }

    public function ajax_atasan()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            
            //panggil dulu library datatablesnya
            $this->load->library('SSP');
            
            //atur nama tablenya disini
            $table = $this->tbpnsxt;

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(

                array('db' => 'p.id',           'dt' => 'id',           'field' => 'id'),
                array('db' => 'p.nip',          'dt' => 'nip',          'field' => 'nip'),
                array('db' => 'p.nip_atasan1',  'dt' => 'nip_atasan1',  'field' => 'nip_atasan1'),
                array('db' => 'p.nip_atasan2',  'dt' => 'nip_atasan2',  'field' => 'nip_atasan2'),
                array('db' => 'p.nip_atasan3',  'dt' => 'nip_atasan3',  'field' => 'nip_atasan3'),
                array('db' => 's.nama_jab as jabatan',     'dt' => 'jabatan',  'field' => 'jabatan'),
                array('db' => 's.loker',     'dt' => 'loker',  'field' => 'loker'),
                array('db' => 'l.kd',          'dt' => 'kd',          'field' => 'kd'),
                array('db' => 'l.nama as namadinas',          'dt' => 'namadinas',          'field' => 'namadinas'),
                array(
                    'db' => 'p.kode_admin',   'dt' => 'kode_admin',
                    'formatter' => function( $d, $row) {
                        if($row['kode_admin']==1)
                        {
                            return 'Admin BKD';
                        }else if($row['kode_admin']==2)
                        {
                            return 'Admin OPD';
                        }else if($row['kode_admin']==4)
                        {
                            return 'PNS';
                        }
                    }, 'field' => 'kode_admin'),
                array('db' => 's.nip',          'dt' => 'nip',          'field' => 'nip'),
                array( 
                    'db' => 'p.id', 'dt' => 'aksi',
                    'formatter' => function( $d, $row) {
                            return '<a class="btn btn-sm btn-primary" href="'.base_url().'dashboard/editatasan/'.$row['id'].'"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;Edit</a>';
                        }, 'field' => 'id'),
            );

            // SQL server connection information
            $sql_details = array(
                'user' => $this->config->item('user'),
                'pass' => $this->config->item('pass'),
                'db'   => $this->config->item('db'),
                'host' => $this->config->item('host')
            );

            $joinQuery = "FROM ".$this->tbpnsxt." AS p, ".$this->tbpns." AS s, ".$this->tbloker." AS l"; 
            $extraWhere= "p.nip=s.nip AND substr(s.loker,1,2)=l.kd";

            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
            );
        }
    }

    public function addatasan()
    {
        $data['title']  = 'Input Data Profil PNS';
        $data['id']     = '';
        $data['pns']    = $this->M_opd->getAll();
        $data['nipstaff']    = $this->M_opd->getBeforeAtasan();
        $data['nip']    = '';
        $data['nip1']   = '';
        $data['nip2']   = '';
        $data['nip3']   = '';
        $data['kodeadmin']  = 4;
        $data['jabatan']  = '';
        $data['password']  = '';
        $this->template->opd('template', 'addatasan', $data);
    }

    public function editatasan($id)
    {
        $id = $this->uri->segment('3');
        $cek = $this->M_opd->cek_atasan($id);
        foreach ($cek->result_array() as $key => $c) {
            $data['id']     = $c['id'];
            $data['nip']    = $c['nip'];
            $data['nip1']   = $c['nip_atasan1'];
            $data['nip2']   = $c['nip_atasan2'];
            $data['nip3']   = $c['nip_atasan3'];
            $data['jabatan']   = $c['nama_jab'];
            $data['loker']   = $c['loker'];
            $data['password'] ='';
            $data['kodeadmin']  = $c['kode_admin'];
        }
        $data['title']  = 'Update Data Profil PNS';
        $data['pns']    = $this->M_opd->getAll();
        $data['unit']    = $this->M_opd->getAllLoker();
        $data['nipstaff']    = $this->M_opd->getAll();
        $this->template->opd('template', 'addatasan', $data);
    }

    public function saveatasan()
    {
        $id         = $this->input->post('id');
        $nip        = $this->input->post('nip');
        $nip1       = $this->input->post('nip1');
        $nip2       = $this->input->post('nip2');
        $nip3       = $this->input->post('nip3');
        $unit       = $this->input->post('unit');
        $jabatan    = $this->input->post('jabatan');
        $password   = $this->input->post('password');
        $kodeadmin  = $this->input->post('kodeadmin');

        if(!empty($password) || $password!=''){
            $this->M_opd->updatasanpswd($id, $nip, $nip1, $nip2, $nip3, $unit, $password, $kodeadmin);
        }else{
            $this->M_opd->updatasan($id, $nip, $nip1, $nip2, $nip3, $unit, $kodeadmin);
        }

    }

    public function skp()
    {
        $data['pns'] = $this->M_opd->getAll();
        $data['bulan'] = date('m');
        $data['tahun'] = date('Y');
        $this->template->opd('template', 'admin_skp', $data);
    }

    public function addskp()
    {
        $data['pns']        = $this->M_opd->getAll();
        $data['kuantitas']  = $this->M_opd->getQuant();
        $data['tahun']      = date('Y');
        $data['title']      = 'Input Data SKP Tahunan';
        $data['id']         = '';
        $data['nip']        = '';
        $data['kegiatan']   = '';
        $data['targetkuantitas'] = '';
        $data['satuankuantitas'] = '';
        $data['targetpenyelesaian'] = '';
        $this->template->opd('template', 'addskp', $data);   
    }

    public function editskp($id)
    {
        $data['title']      = 'Edit Data SKP Tahunan';
        $data['pns']        = $this->M_opd->getAll();
        $data['kuantitas']  = $this->M_opd->getQuant();
        $ambil              = $this->M_opd->getById($id);
        foreach ($ambil->result_array() as $key => $v) {
            $data['id']        = $v['id_skp'];
            $data['tahun']      = $v['tahun'];
            $data['nip']        = $v['nip'];
            $data['kegiatan']   = $v['kegiatan'];
            $data['targetkuantitas'] = $v['target_kuantitas'];
            $data['satuankuantitas'] = $v['satuan_kuantitas'];
            $data['targetpenyelesaian'] = $v['target_selesai'];
        }
        $this->template->opd('template', 'addskp', $data);   
    }

    public function kskp()
    {
        $this->template->opd('template', 'admin_kskp');
    }

    public function addkskp()
    {
        $data['title']   = 'Input Data Laporan Kinerja SKP';
        $data['tanggal'] = date('d-m-Y H:i:s');
        $data['pns']     = $this->M_opd->getAll();
        $data['id']      = '';
        $data['error']   = '';
        $data['getskp']  = '';
        $data['nip']     = '';
        $data['skp']     = '';
        $data['kegiatan']= '-';
        $data['kuantitas']  = '';
        $data['satuan']  = '';
        $data['status']  = '-';
        $data['sesuai']  = '-';
        $this->template->opd('template', 'addkskp', $data);      
    }

    public function editkskp($id)
    {
        $data['title']      = 'Edit Data Laporan Kinerja SKP';
        $data['pns']        = $this->M_opd->getAll();
        $ambil              = $this->M_opd->getKskpById($id);
        foreach ($ambil->result_array() as $key => $v) {
            $data['id']         = $v['id_lap_skp'];
            $waktu = $v['tanggal'];
            $tgl = substr($waktu, 8,2);
            $bln = substr($waktu, 5,2);
            $thn = substr($waktu, 0,4);
            $jam = substr($waktu, -9);
            $time = $tgl.'-'.$bln.'-'.$thn.$jam;
            $data['tanggal']    = $time;
            $data['nip']        = $v['nip'];
            $data['skp']        = $v['id_skp'];
            $data['numsatuan']  = $v['numsatuan'];
            $data['satuan']     = $v['satuan'];
            $data['kuantitas']  = $v['tercapai'];
            $data['kegiatan']   = $v['pekerjaan'];
            $data['status']     = $v['status'];
            $data['sesuai']     = $v['sesuai'];
            $nip = $v['nip'];
        }
        $data['getskp']              = $this->M_opd->getskptahunan($nip);
        $data['error'] ='';
        $this->template->opd('template', 'addkskp', $data);   
    }

    public function kprod()
    {
        $this->template->opd('template', 'admin_kprod');
    }

    public function addkprod()
    {
        $data['title']   = 'Input Data Laporan Kinerja Produktivitas';
        $data['tanggal'] = date('d-m-Y H:i:s');
        $data['pns']     = $this->M_opd->getAll();
        $data['id']      = '';
        $data['error']   = '';
        $data['getskp']  = '';
        $data['nip']     = '';
        $data['kegiatan']= '-';
        $data['kuantitas']  = '';
        $data['satuankuantitas']  = '';
        $data['satkuantitas']  = $this->M_opd->getQuant();
        $data['status']  = '-';
        $data['sesuai']  = '-';
        $this->template->opd('template', 'addkprod', $data);      
    }

    public function editkprod($id)
    {
        $data['title']      = 'Edit Data Laporan Kinerja Produktivitas';
        $data['pns']        = $this->M_opd->getAll();
        $ambil              = $this->M_opd->getKprodById($id);
        foreach ($ambil->result_array() as $key => $v) {
            $data['id']         = $v['id_lap_prod'];
            $waktu = $v['tanggal'];
            $tgl = substr($waktu, 8,2);
            $bln = substr($waktu, 5,2);
            $thn = substr($waktu, 0,4);
            $jam = substr($waktu, -9);
            $time = $tgl.'-'.$bln.'-'.$thn.$jam;
            $data['tanggal']    = $time;
            $data['nip']        = $v['nip'];
            $data['kegiatan']   = $v['kegiatan'];
            $data['kuantitas']  = $v['qty'];
            $data['satuankuantitas']     = $v['satuan'];
            $data['satkuantitas']  = $this->M_opd->getQuant();
            $data['status']     = $v['status'];
            $nip = $v['nip'];
        }
        $data['error'] ='';
        $this->template->opd('template', 'addkprod', $data);   
    }

    public function acc()
    {
        $this->template->opd('template', 'admin_acc');
    }

    public function ajax_skp()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            
            //panggil dulu library datatablesnya
            $this->load->library('SSP');
            
            //atur nama tablenya disini
            $table = $this->tbskp;

            // Table's primary key
            $primaryKey = 'id_skp';

            $columns = array(

                array('db' => 's.nip',          'dt' => 'nip',          'field' => 'nip'),
                array('db' => 's.tahun',        'dt' => 'tahun',        'field' => 'tahun'),
                array('db' => 'p.nama',         'dt' => 'nama',        'field' => 'nama'),
                array('db' => 's.kegiatan',     'dt' => 'kegiatan',      'field' => 'kegiatan'),
                array('db' => 'q.id',     'dt' => 'id_kuantitas',      'field' => 'id'),
                array('db' => 'q.kuantitas',     'dt' => 's_kuantitas',      'field' => 'kuantitas'),
                array('db' => 's.satuan_kuantitas','dt' => 'satuan_kuantitas', 'field' => 'satuan_kuantitas'),
                array('db' => 's.target_kuantitas','dt' => 't_kuantitas', 'field' => 'target_kuantitas'),
                array(
                    'db' => 's.target_selesai',  'dt' => 't_selesai', 
                    'formatter' => function( $d, $row) {
                                        return $row['target_selesai'].' Bulan';
                                    },'field' => 'target_selesai'),
                array( 
                    'db' => 's.id_skp', 'dt' => 'aksi',
                    'formatter' => function( $d, $row) {
                            //return '<a class="btn btn-sm btn-primary" href="javascript:void(0)" onclick="edit_pns('."'".$d."','".$row['id_skp']."'".')"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;Edit</a>';
                        return '<a class="btn btn-sm btn-primary" href="'.base_url().'dashboard/editskp/'.$row['id_skp'].'"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;Edit</a>';
                        }, 'field' => 'id_skp'),
            );

            // SQL server connection information
            $sql_details = array(
                'user' => $this->config->item('user'),
                'pass' => $this->config->item('pass'),
                'db'   => $this->config->item('db'),
                'host' => $this->config->item('host')
            );

            $joinQuery = "FROM ".$this->tbskp." s JOIN ".$this->tbpns." p JOIN ".$this->tbquant." q"; 
            $extraWhere= "s.nip=p.nip AND s.satuan_kuantitas=q.id";

            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
            );
        }
    }

    public function ajax_kskp()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            
            //panggil dulu library datatablesnya
            $this->load->library('SSP');
            
            //atur nama tablenya disini
            $table = $this->tbskp;

            // Table's primary key
            $primaryKey = 'id_skp';

            $columns = array(

                array('db' => 'a.id_skp',          'dt' => 'id_skp',          'field' => 'id_skp'),
                array('db' => 'a.nip',          'dt' => 'nip',          'field' => 'nip'),
                array('db' => 'date_format(c.tanggal, "%d-%m-%Y") as tanggal',    'dt' => 'tanggal',      'field' => 'tanggal'),
                array('db' => 'TIME(c.tanggal) as waktu',      'dt' => 'waktu',      'field' => 'waktu'),
                array('db' => 'a.kegiatan as skptahunan',     'dt' => 'skptahunan',     'field' => 'skptahunan'),
                array('db' => 'c.kegiatan',     'dt' => 'kegiatan',     'field' => 'kegiatan'),
                array('db' => 'a.target_kuantitas',  'dt' => 'target',  'field' => 'target_kuantitas'),
                array('db' => 'k.id',           'dt' => 'numsatuan',    'field' => 'id'),
                array('db' => 'k.kuantitas as satuan',    'dt' => 'satuan',       'field' => 'satuan'),
                array('db' => 'c.kuantitas',    'dt' => 'tercapai',     'field' => 'kuantitas'),
                array('db' => 'a.tahun',        'dt' => 'tahun',   'field' => 'tahun'),
                array('db' => 'c.id_skp_tahunan','dt' => 'id_skp_tahunan','field' => 'id_skp_tahunan'),
                array('db' => 'c.sesuai',       'dt' => 'sesuai',        'field' => 'sesuai'),
                array('db' => 'c.status',       'dt' => 'status',        'field' => 'status'),
                array( 
                    'db' => 'c.id_lap_skp', 'dt' => 'aksi',
                    'formatter' => function( $d, $row) {
                            //return '<a class="btn btn-sm btn-primary" href="javascript:void(0)" onclick="edit_pns('."'".$d."','".$row['id_lap_skp']."'".')"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;Edit</a>';
                        return '<a class="btn btn-sm btn-primary" href="'.base_url().'dashboard/editkskp/'.$row['id_lap_skp'].'"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;Edit</a>';
                        }, 'field' => 'id_lap_skp'),
            );

            // SQL server connection information
            $sql_details = array(
                'user' => $this->config->item('user'),
                'pass' => $this->config->item('pass'),
                'db'   => $this->config->item('db'),
                'host' => $this->config->item('host')
            );

            $joinQuery = "FROM ".$this->tbskp." AS a, ".$this->tbquant." AS k, ".$this->tbkskp." AS c"; 
            $extraWhere= "a.satuan_kuantitas=k.id AND a.id_skp=c.id_skp_tahunan";


            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
            );
        }
    }

    public function ajax_kprod()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            
            //panggil dulu library datatablesnya
            $this->load->library('SSP');
            
            //atur nama tablenya disini
            $table = $this->tbkprod;

            // Table's primary key
            $primaryKey = 'id_lap_prod';

            $columns = array(

                array('db' => 'p.nip',          'dt' => 'nip',          'field' => 'nip'),
                array('db' => 'p.tanggal',      'dt' => 'tanggal',      'field' => 'tanggal'),
                array('db' => 'p.kegiatan',     'dt' => 'kegiatan',     'field' => 'kegiatan'),
                array('db' => 'p.kuantitas',    'dt' => 'kuantitas',    'field' => 'kuantitas'),
                array('db' => 'p.satuan',       'dt' => 'satuankuantitas',       'field' => 'satuan'),
                array('db' => 'k.id as numsatuan',           'dt' => 'numsatuan',    'field' => 'numsatuan'),
                array('db' => 'k.kuantitas as satkuantitas',    'dt' => 'satkuantitas',       'field' => 'satkuantitas'),
                array('db' => 'p.status',       'dt' => 'status',       'field' => 'status'),
                array( 
                    'db' => 'id_lap_prod', 'dt' => 'aksi',
                    'formatter' => function( $d, $row) {
                            return '<a class="btn btn-sm btn-primary" href="'.base_url().'dashboard/editkprod/'.$row['id_lap_prod'].'"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;Edit</a>';
                        }, 'field' => 'id_lap_prod'),
            );

            // SQL server connection information
            $sql_details = array(
                'user' => $this->config->item('user'),
                'pass' => $this->config->item('pass'),
                'db'   => $this->config->item('db'),
                'host' => $this->config->item('host')
            );

            $joinQuery = "FROM ".$this->tbkprod." AS p, ".$this->tbquant." AS k"; 
            $extraWhere= "p.satuan=k.id";


            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
            );
        }
    }

    public function ajax_acc()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            
            //panggil dulu library datatablesnya
            $this->load->library('SSP');
            
            //atur nama tablenya disini
            $table = $this->tbacc;

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(

                array('db' => 'p.nip',          'dt' => 'nip',          'field' => 'nip'),
                array('db' => 'p.nip_atasan',   'dt' => 'nip_atasan',   'field' => 'nip_atasan'),
                array('db' => 'p.blth',         'dt' => 'blth',      'field' => 'blth'),
                array('db' => 'p.status',       'dt' => 'status',       'field' => 'status'),
                array('db' => 'p.tgl_persetujuan',     'dt' => 'tgl_persetujuan',     'field' => 'tgl_persetujuan'),
                array('db' => 'p.jml_kegiatan',    'dt' => 'jml_kegiatan',    'field' => 'jml_kegiatan'),
                array('db' => 'p.sumber_data',       'dt' => 'sumber_data',       'field' => 'sumber_data'),
                
                array( 
                    'db' => 'id', 'dt' => 'aksi',
                    'formatter' => function( $d, $row) {
                            return '<a class="btn btn-sm btn-primary" href="javascript:void(0)" onclick="edit_pns('."'".$d."','".$row['id']."'".')"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;Edit</a>';
                        }, 'field' => 'id'),
            );

            // SQL server connection information
            $sql_details = array(
                'user' => $this->config->item('user'),
                'pass' => $this->config->item('pass'),
                'db'   => $this->config->item('db'),
                'host' => $this->config->item('host')
            );

            $joinQuery = "FROM ".$this->tbacc." AS p"; 
            $extraWhere= "p.nip is not null";


            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
            );
        }
    }

    public function note()
    {
        $this->template->opd('template', 'catatan');
    }

    public function ajax_note()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            
            //panggil dulu library datatablesnya
            $this->load->library('SSP');
            
            //atur nama tablenya disini
            $table = $this->tbnote;

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(

                array('db' => 'p.nip',          'dt' => 'nip',          'field' => 'nip'),
                array('db' => 'a.nama',         'dt' => 'nama',          'field' => 'nama'),
                array('db' => 'p.nip_atasan',   'dt' => 'nip_atasan',   'field' => 'nip_atasan'),
                array('db' => 'p.blth',         'dt' => 'blth',      'field' => 'blth'),
                array('db' => 'p.tgl_pembuatan',     'dt' => 'tgl_pembuatan',     'field' => 'tgl_pembuatan'),
                array('db' => 'p.tgl_edit',    'dt' => 'tgl_edit',    'field' => 'tgl_edit'),                
                array( 
                    'db' => 'p.id', 'dt' => 'aksi',
                    'formatter' => function( $d, $row) {
                            return '
                                    <a class="btn btn-sm btn-success" href="javascript:void(0)" onclick="detail_note('."'".$d."','".$row['id']."'".')"><i class="glyphicon glyphicon-eye-open"></i></a>
                                    <a class="btn btn-sm btn-warning" href="'.base_url().'opd/printnote/'.$row['id'].'" target="_blank"><i class="glyphicon glyphicon-print"></i></a>';
                        }, 'field' => 'id'),
            );

            // SQL server connection information
            $sql_details = array(
                'user' => $this->config->item('user'),
                'pass' => $this->config->item('pass'),
                'db'   => $this->config->item('db'),
                'host' => $this->config->item('host')
            );

            $joinQuery = "FROM ".$this->tbnote." AS p, ".$this->tbpns." AS a"; 
            $extraWhere= "p.nip=a.nip";


            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
            );
        }
    }
}
