<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->library(array('session','Myauth'));
        $lvl = $this->session->userdata('userkodeadmin');
        if( $lvl == '1')
        {
            redirect('dashboard');
        }
        else if( $lvl == '2')
        {
            redirect('opd');
        }
        else if( $lvl == '3')
        {
            redirect('ats');
        }
        else if( $lvl == '4')
        {
            redirect('opd');
        }
    }

	public function index()
	{
        $this->template->visitor('template', 'visitor');
    }
}
