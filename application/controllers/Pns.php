<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pns extends CI_Controller {

    protected $tblibur    = 'libur';
    protected $tbpns    = 'tbl_pns';
    protected $tbeselon = 'ref_eselon';
    protected $tbgol    = 'ref_golongan';
    protected $tbskp    = 'skp_tahunan';
    protected $tbquant  = 'kuantitas';
    protected $tbkskp   = 'lap_kinerja_skp';
    protected $tbkprod  = 'lap_kinerja_produktivitas';
    protected $tbacc    = 'persetujuan_atasan';
    protected $tbnote    = 'penilaian_perilaku';

	function __construct(){
        parent::__construct();
        $this->load->library(array('session','Myauth'));
        $this->load->model(array('M_opd','M_pns'));
        $cek = $this->session->userdata('userkodeadmin');
        if( $cek=='1' || $cek=='2' || $cek=='')
        {
            echo'<script>alert("Anda tidak diperkenankan mengakses halaman ini. Hubungi Administrator !");window.location.href = "'.base_url('login').'";</script>';       
        }
    }

	public function index()
	{
        $this->template->pns('template', 'dashpns');
    }

    public function pnsnote()
    {
        $this->template->opd('template', 'pnscatatan');
    }

    public function pnskprod()
    {
        $this->template->opd('template', 'pnskprod');
    }

    public function ajax_kprod()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            
            //panggil dulu library datatablesnya
            $this->load->library('SSP');
            
            //atur nama tablenya disini
            $table = $this->tbkprod;

            // Table's primary key
            $primaryKey = 'id_lap_prod';

            $columns = array(

                array('db' => 'p.nip',          'dt' => 'nip',          'field' => 'nip'),
                array('db' => 'p.tanggal',      'dt' => 'tanggal',      'field' => 'tanggal'),
                array('db' => 'p.kegiatan',     'dt' => 'kegiatan',     'field' => 'kegiatan'),
                array('db' => 'p.kuantitas',    'dt' => 'kuantitas',    'field' => 'kuantitas'),
                array('db' => 'p.satuan',       'dt' => 'satuankuantitas',       'field' => 'satuan'),
                array('db' => 'k.id',           'dt' => 'numsatuan',    'field' => 'id'),
                array('db' => 'k.kuantitas as satkuantitas',    'dt' => 'satkuantitas',       'field' => 'satkuantitas'),
                array('db' => 'p.status',       'dt' => 'status',       'field' => 'status'),
                array( 
                    'db' => 'id_lap_prod', 'dt' => 'aksi',
                    'formatter' => function( $d, $row) {
                            return '<a class="btn btn-sm btn-primary" href="'.base_url().'pns/editkprod/'.$row['id_lap_prod'].'"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;Edit</a>';
                        }, 'field' => 'id_lap_prod'),
            );

            // SQL server connection information
            $sql_details = array(
                'user' => $this->config->item('user'),
                'pass' => $this->config->item('pass'),
                'db'   => $this->config->item('db'),
                'host' => $this->config->item('host')
            );

            $joinQuery = "FROM ".$this->tbkprod." AS p, ".$this->tbquant." AS k"; 
            $extraWhere= "p.satuan=k.id AND p.nip='".$this->session->userdata('userid')."'";


            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
            );
        }
    }

    public function addkprod()
    {
        $data['title']   = 'Input Data Laporan Kinerja Produktivitas';
        $data['tanggal'] = date('d-m-Y H:i:s');
        $data['pns']     = $this->M_opd->getAll();
        $data['id']      = '';
        $data['error']   = '';
        $data['getskp']  = '';
        $data['nip']     = $this->session->userdata('userid');
        $data['kegiatan']= '-';
        $data['kuantitas']  = '';
        $data['satuankuantitas']  = '';
        $data['satkuantitas']  = $this->M_opd->getQuant();
        $data['status']  = '-';
        $data['sesuai']  = '-';
        $this->template->opd('template', 'pnsaddkprod', $data);      
    }

    public function editkprod($id)
    {
        $data['title']      = 'Edit Data Laporan Kinerja Produktivitas';
        $data['pns']        = $this->M_opd->getAll();
        $ambil              = $this->M_opd->getKprodById($id);
        foreach ($ambil->result_array() as $key => $v) {
            $data['id']         = $v['id_lap_prod'];
            $waktu = $v['tanggal'];
            $tgl = substr($waktu, 8,2);
            $bln = substr($waktu, 5,2);
            $thn = substr($waktu, 0,4);
            $jam = substr($waktu, -9);
            $time = $tgl.'-'.$bln.'-'.$thn.$jam;
            $data['tanggal']    = $time;
            $data['nip']        = $v['nip'];
            $data['kegiatan']   = $v['kegiatan'];
            $data['kuantitas']  = $v['qty'];
            $data['satuankuantitas']     = $v['satuan'];
            $data['satkuantitas']  = $this->M_opd->getQuant();
            $data['status']     = $v['status'];
            $nip = $v['nip'];
        }
        $data['error'] ='';
        $this->template->opd('template', 'pnsaddkprod', $data);   
    }

    public function savekprod()
    {
        $id = $this->input->post('id');
        $tanggal = $this->input->post('tanggal');
        $nip = $this->input->post('nip');
        $kegiatan = $this->input->post('kegiatan');
        $kuantitas = $this->input->post('kuantitas');
        $satuan = $this->input->post('satuankuantitas');
        $status = $this->input->post('status');

        $tgl = substr($tanggal, 0,2);
        $bln = substr($tanggal, 3,2);
        $thn = substr($tanggal, 6,4);
        $ambiltgl = $thn.'-'.$bln.'-'.$tgl;
        $cek = $this->M_opd->ceklibur($ambiltgl);
        if($cek->num_rows()>0)
        {
            $data['title']      = 'Edit Data Laporan Kinerja Produktivitas';
            $data['pns']        = $this->M_opd->getAll();
                $data['id']         = $this->input->post('id');
                $data['tanggal']    = $this->input->post('tanggal');
                $data['nip']        = $this->input->post('nip');
                $data['satkuantitas']  = $this->M_opd->getQuant();
                $data['satuankuantitas']     = $this->input->post('satuankuantitas');
                $data['kuantitas']  = $this->input->post('kuantitas');
                $data['kegiatan']   = $this->input->post('kegiatan');
                $data['status']     = $this->input->post('status');
                $nip = $this->input->post('nip');
                foreach ($cek->result_array() as $key => $c) {
                    $data['error'] =$c['keterangan'];
                }
            $this->template->opd('template', 'pnsaddkprod', $data);   
        }else{
            if($id>0){
                $upd = $this->M_pns->edit_lap_kprod($id, $tanggal, $nip, $kegiatan, $kuantitas, $satuan, $status);
            }else{
                $ins = $this->M_pns->save_lap_kprod($id, $tanggal, $nip, $kegiatan, $kuantitas, $satuan, $status);
            }
        }
    }

    public function pnskskp()
    {
        $this->template->opd('template', 'pnskskp');
    }

    public function ajax_kskp()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            
            //panggil dulu library datatablesnya
            $this->load->library('SSP');
            
            //atur nama tablenya disini
            $table = $this->tbskp;

            // Table's primary key
            $primaryKey = 'id_skp';

            $columns = array(

                array('db' => 'a.nip',          'dt' => 'nip',          'field' => 'nip'),
                array('db' => 'date_format(c.tanggal, "%d-%m-%Y") as tanggal',    'dt' => 'tanggal',      'field' => 'tanggal'),
                array('db' => 'TIME(c.tanggal) as waktu',      'dt' => 'waktu',      'field' => 'waktu'),
                array('db' => 'a.kegiatan as skptahunan',     'dt' => 'skptahunan',     'field' => 'skptahunan'),
                array('db' => 'c.kegiatan',     'dt' => 'kegiatan',     'field' => 'kegiatan'),
                array('db' => 'a.target_kuantitas',  'dt' => 'target',  'field' => 'target_kuantitas'),
                array('db' => 'k.id',           'dt' => 'numsatuan',    'field' => 'id'),
                array('db' => 'k.kuantitas as satuan',    'dt' => 'satuan',       'field' => 'satuan'),
                array('db' => 'c.kuantitas',    'dt' => 'tercapai',     'field' => 'kuantitas'),
                array('db' => 'a.tahun',        'dt' => 'tahun',   'field' => 'tahun'),
                array('db' => 'c.id_skp_tahunan','dt' => 'id_skp_tahunan','field' => 'id_skp_tahunan'),
                array('db' => 'c.sesuai',       'dt' => 'sesuai',        'field' => 'sesuai'),
                array('db' => 'c.status',       'dt' => 'status',        'field' => 'status'),
                array( 
                    'db' => 'c.id_lap_skp', 'dt' => 'aksi',
                    'formatter' => function( $d, $row) {
                            //return '<a class="btn btn-sm btn-primary" href="javascript:void(0)" onclick="edit_pns('."'".$d."','".$row['id_lap_skp']."'".')"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;Edit</a>';
                        return '<a class="btn btn-sm btn-primary" href="'.base_url().'pns/editkskp/'.$row['id_lap_skp'].'"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;Edit</a>';
                        }, 'field' => 'id_lap_skp'),
            );

            // SQL server connection information
            $sql_details = array(
                'user' => $this->config->item('user'),
                'pass' => $this->config->item('pass'),
                'db'   => $this->config->item('db'),
                'host' => $this->config->item('host')
            );

            $joinQuery = "FROM ".$this->tbskp." AS a, ".$this->tbquant." AS k, ".$this->tbkskp." AS c"; 
            $extraWhere= "a.satuan_kuantitas=k.id AND a.id_skp=c.id_skp_tahunan AND a.nip='".$this->session->userdata('userid')."'";


            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
            );
        }
    }

    public function addkskp()
    {
        $data['title']   = 'Input Data Laporan Kinerja SKP';
        $data['tanggal'] = date('d-m-Y H:i:s');
        $data['pns']     = $this->M_opd->getAll();
        $data['id']      = '';
        $data['error']   = '';
        $data['getskp']  = $this->M_opd->getskptahunan($this->session->userdata('userid'));
        $data['nip']     = $this->session->userdata('userid');
        $data['skp']     = '';
        $data['kegiatan']= '-';
        $data['kuantitas']  = '';
        $data['satuan']  = '';
        $data['status']  = '-';
        $data['sesuai']  = '-';
        $this->template->opd('template', 'pnsaddkskp', $data);      
    }

    public function editkskp($id)
    {
        $data['title']      = 'Edit Data Laporan Kinerja SKP';
        $data['pns']        = $this->M_opd->getAll();
        $ambil              = $this->M_opd->getKskpById($id);
        foreach ($ambil->result_array() as $key => $v) {
            $data['id']         = $v['id_lap_skp'];
            $waktu = $v['tanggal'];
            $tgl = substr($waktu, 8,2);
            $bln = substr($waktu, 5,2);
            $thn = substr($waktu, 0,4);
            $jam = substr($waktu, -9);
            $time = $tgl.'-'.$bln.'-'.$thn.$jam;
            $data['tanggal']    = $time;
            $data['nip']        = $v['nip'];
            $data['skp']        = $v['id_skp'];
            $data['numsatuan']  = $v['numsatuan'];
            $data['satuan']     = $v['satuan'];
            $data['kuantitas']  = $v['tercapai'];
            $data['kegiatan']   = $v['pekerjaan'];
            $data['status']     = $v['status'];
            $data['sesuai']     = $v['sesuai'];
            $nip = $v['nip'];
        }
        $data['getskp']              = $this->M_opd->getskptahunan($nip);
        $data['error'] ='';
        $this->template->opd('template', 'pnsaddkskp', $data);   
    }

    public function savekskp()
    {
        $id = $this->input->post('id');
        $tanggal = $this->input->post('tanggal');
        $nip = $this->input->post('nip');
        $id_skp = $this->input->post('skp');
        $kegiatan = $this->input->post('kegiatan');
        $kuantitas = $this->input->post('kuantitas');
        $satuan = $this->input->post('numsatuan');
        $status = $this->input->post('status');
        $sesuai = $this->input->post('sesuai');
        //echo $id;

        $tgl = substr($tanggal, 0,2);
        $bln = substr($tanggal, 3,2);
        $thn = substr($tanggal, 6,4);
        $ambiltgl = $thn.'-'.$bln.'-'.$tgl;
        $cek = $this->M_opd->ceklibur($ambiltgl);
        if($cek->num_rows()>0)
        {
            $data['title']      = 'Edit Data Laporan Kinerja SKP';
            $data['pns']        = $this->M_opd->getAll();
                $data['id']         = $this->input->post('id');
                $data['tanggal']    = $this->input->post('tanggal');
                $data['nip']        = $this->input->post('nip');
                $data['skp']        = $this->input->post('skp');
                $data['numsatuan']  = $this->input->post('numsatuan');
                $data['satuan']     = $this->input->post('satuan');
                $data['kuantitas']  = $this->input->post('kuantitas');
                $data['kegiatan']   = $this->input->post('kegiatan');
                $data['status']     = $this->input->post('status');
                $data['sesuai']     = $this->input->post('sesuai');
                $nip = $this->input->post('nip');
                $data['getskp']              = $this->M_opd->getskptahunan($nip);
                foreach ($cek->result_array() as $key => $c) {
                    $data['error'] =$c['keterangan'];
                }
            $this->template->opd('template', 'pnsaddkskp', $data);   
        }else{
            if($id>0){
                $upd = $this->M_pns->edit_lap_skp($id, $tanggal, $nip, $id_skp, $kegiatan, $kuantitas, $satuan, $status, $sesuai);
            }else{
                $ins = $this->M_pns->save_lap_skp($id, $tanggal, $nip, $id_skp, $kegiatan, $kuantitas, $satuan, $status, $sesuai);
            }
        }
    }

    public function pnsskp()
    {
        $data['pns'] = $this->session->userdata('userid');
        $data['bulan'] = date('m');
        $data['tahun'] = date('Y');
        $this->template->opd('template', 'pnsskp', $data);
    }

    public function ajax_skp()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            
            //panggil dulu library datatablesnya
            $this->load->library('SSP');
            
            //atur nama tablenya disini
            $table = $this->tbskp;

            // Table's primary key
            $primaryKey = 'id_skp';

            $columns = array(

                array('db' => 's.nip',          'dt' => 'nip',          'field' => 'nip'),
                array('db' => 's.tahun',        'dt' => 'tahun',        'field' => 'tahun'),
                array('db' => 'p.nama',         'dt' => 'nama',        'field' => 'nama'),
                array('db' => 's.kegiatan',     'dt' => 'kegiatan',      'field' => 'kegiatan'),
                array('db' => 'q.id',     'dt' => 'id_kuantitas',      'field' => 'id'),
                array('db' => 'q.kuantitas',     'dt' => 's_kuantitas',      'field' => 'kuantitas'),
                array('db' => 's.satuan_kuantitas','dt' => 'satuan_kuantitas', 'field' => 'satuan_kuantitas'),
                array('db' => 's.target_kuantitas','dt' => 't_kuantitas', 'field' => 'target_kuantitas'),
                array(
                    'db' => 's.target_selesai',  'dt' => 't_selesai', 
                    'formatter' => function( $d, $row) {
                                        return $row['target_selesai'].' Bulan';
                                    },'field' => 'target_selesai'),
                array( 
                    'db' => 's.id_skp', 'dt' => 'aksi',
                    'formatter' => function( $d, $row) {
                            //return '<a class="btn btn-sm btn-primary" href="javascript:void(0)" onclick="edit_pns('."'".$d."','".$row['id_skp']."'".')"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;Edit</a>';
                        return '<a class="btn btn-sm btn-primary" href="'.base_url().'pns/editskp/'.$row['id_skp'].'"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;Edit</a>';
                        }, 'field' => 'id_skp'),
            );

            // SQL server connection information
            $sql_details = array(
                'user' => $this->config->item('user'),
                'pass' => $this->config->item('pass'),
                'db'   => $this->config->item('db'),
                'host' => $this->config->item('host')
            );

            $joinQuery = "FROM ".$this->tbskp." s JOIN ".$this->tbpns." p JOIN ".$this->tbquant." q"; 
            $extraWhere= "s.nip=p.nip AND s.satuan_kuantitas=q.id AND s.nip='".$this->session->userdata('userid')."'";

            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
            );
        }
    }

    public function addskp()
    {
        $data['pns']        = $this->M_opd->getAll();
        $data['kuantitas']  = $this->M_opd->getQuant();
        $data['tahun']      = date('Y');
        $data['title']      = 'Input Data SKP Tahunan';
        $data['id']         = '';
        $data['nip']        = $this->session->userdata('userid');
        $data['kegiatan']   = '';
        $data['targetkuantitas'] = '';
        $data['satuankuantitas'] = '';
        $data['targetpenyelesaian'] = '';
        $this->template->opd('template', 'pnsaddskp', $data);   
    }

    public function editskp($id)
    {
        $data['title']      = 'Edit Data SKP Tahunan';
        $data['pns']        = $this->M_opd->getAll();
        $data['kuantitas']  = $this->M_opd->getQuant();
        $ambil              = $this->M_opd->getById($id);
        foreach ($ambil->result_array() as $key => $v) {
            $data['id']        = $v['id_skp'];
            $data['tahun']      = $v['tahun'];
            $data['nip']        = $v['nip'];
            $data['kegiatan']   = $v['kegiatan'];
            $data['targetkuantitas'] = $v['target_kuantitas'];
            $data['satuankuantitas'] = $v['satuan_kuantitas'];
            $data['targetpenyelesaian'] = $v['target_selesai'];
        }
        $this->template->opd('template', 'pnsaddskp', $data);   
    }

    public function saveskp()
    {
        $id = $this->input->post('id');
        $nip = $this->input->post('nip');
        $tahun = $this->input->post('tahun');
        $kegiatan = $this->input->post('kegiatan');
        $targetkuantitas = $this->input->post('targetkuantitas');
        $satuankuantitas = $this->input->post('satuankuantitas');
        $targetpenyelesaian = $this->input->post('targetpenyelesaian');
        //echo $id;

        if($id>0){
            $upd = $this->M_pns->editSkp($id, $nip, $tahun, $kegiatan, $targetkuantitas, $satuankuantitas, $targetpenyelesaian);
        }else{
            $ins = $this->M_pns->saveSkp($id, $nip, $tahun, $kegiatan, $targetkuantitas, $satuankuantitas, $targetpenyelesaian);
        }
    }
}
