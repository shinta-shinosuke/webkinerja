<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ats extends CI_Controller {

    protected $tblibur    = 'libur';
    protected $tbpns    = 'tbl_pns';
    protected $tbeselon = 'ref_eselon';
    protected $tbgol    = 'ref_golongan';
    protected $tbskp    = 'skp_tahunan';
    protected $tbquant  = 'kuantitas';
    protected $tbkskp   = 'lap_kinerja_skp';
    protected $tbkprod  = 'lap_kinerja_produktivitas';
    protected $tbacc    = 'persetujuan_atasan';
    protected $tbnote    = 'penilaian_perilaku';

	function __construct(){
        parent::__construct();
        $this->load->library(array('session','Myauth'));
        $this->load->model(array('M_opd'));
        $cek = $this->session->userdata('userkodeadmin');
        if( $cek=='1' || $cek=='2' || $cek=='')
        {
            echo'<script>alert("Anda tidak diperkenankan mengakses halaman ini. Hubungi Administrator !");window.location.href = "'.base_url('login').'";</script>';       
        }
    }

	public function index()
	{
        $this->template->opd('template', 'ats');
    }

    public function note()
    {
        $this->template->opd('template', 'catatan');
    }

    public function addnote()
    {
        $data['title']          = 'Input Data Penilaian Perilaku';
        $data['id']             ='';
        $data['pns']            = $this->M_opd->getStaff($this->session->userdata('userid'));
        $data['nip']            ='';
        $data['nip_atasan']     = $this->session->userdata('userid');
        $data['blth']           = date('m').date('Y');
        $data['pelayanan']      ='';
        $data['integritas']     ='';
        $data['komitmen']       ='';
        $data['disiplin']       ='';
        $data['kerjasama']      ='';
        $data['kepemimpinan']   ='';
        $data['tanggal']        =date('Y-m-d H:i:s');
        $data['tgl_edit']       =date('Y-m-d H:i:s');
        $this->template->opd('template', 'addcatatan', $data);       
    }

    public function editnote($id)
    {
        $data['title']          = 'EDit Data Penilaian Perilaku';
        $getnote = $this->M_opd->getNote($id);
        foreach ($getnote->result_array() as $key => $v) {
            $data['id']             =$v['id'];    
            $data['nip']            =$v['nip'];
            $data['blth']           =$v['blth'];
            $data['pelayanan']      =$v['pelayanan'];
            $data['integritas']     =$v['integritas'];
            $data['komitmen']       =$v['komitmen'];
            $data['disiplin']       =$v['disiplin'];
            $data['kerjasama']      =$v['kerjasama'];
            $data['kepemimpinan']   =$v['kepemimpinan'];
        }
        
        $data['pns']            = $this->M_opd->getAll();
        $data['nip_atasan']     = $this->session->userdata('userid');
        $this->template->opd('template', 'addcatatan', $data);       
    }

    public function savenote()
    {
        $id              = $this->input->post('id');
        $nip             = $this->input->post('nipstaf');
        $nip_atasan      = $this->session->userdata('userid');
        $blth            = date('m').date('Y');
        $pelayanan       = $this->input->post('pelayanan');
        $integritas      = $this->input->post('integritas');
        $komitmen        = $this->input->post('komitmen');
        $disiplin        = $this->input->post('disiplin');
        $kerjasama       = $this->input->post('kerjasama');
        $kepemimpinan    = $this->input->post('kepemimpinan');
        $tanggal         = date('Y-m-d H:i:s');
        $tgl_edit        = date('Y-m-d H:i:s');
        
        if($id>0){
            $this->M_opd->updnote($id, $nip, $nip_atasan, $blth, $pelayanan, $integritas, $komitmen, $disiplin, $kerjasama, $kepemimpinan, $tanggal, $tgl_edit);
        }else{
            $this->M_opd->insnote($id, $nip, $nip_atasan, $blth, $pelayanan, $integritas, $komitmen, $disiplin, $kerjasama, $kepemimpinan, $tanggal, $tgl_edit);
        }
        
    }

    public function ajax_note()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            
            //panggil dulu library datatablesnya
            $this->load->library('SSP');
            
            //atur nama tablenya disini
            $table = $this->tbnote;

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(

                array('db' => 'p.nip',          'dt' => 'nip',          'field' => 'nip'),
                array('db' => 'a.nama',         'dt' => 'nama',          'field' => 'nama'),
                array('db' => 'p.nip_atasan',   'dt' => 'nip_atasan',   'field' => 'nip_atasan'),
                array('db' => 'p.blth',         'dt' => 'blth',      'field' => 'blth'),
                array('db' => 'p.tgl_pembuatan',     'dt' => 'tgl_pembuatan',     'field' => 'tgl_pembuatan'),
                array('db' => 'p.tgl_edit',    'dt' => 'tgl_edit',    'field' => 'tgl_edit'),                
                array( 
                    'db' => 'p.id', 'dt' => 'aksi',
                    'formatter' => function( $d, $row) {
                            return '<a class="btn btn-sm btn-primary" href="'.base_url().'ats/editnote/'.$row['id'].'"><i class="glyphicon glyphicon-pencil"></i></a>
                                    <a class="btn btn-sm btn-success" href="javascript:void(0)" onclick="detail_note('."'".$d."','".$row['id']."'".')"><i class="glyphicon glyphicon-eye-open"></i></a>
                                    <a class="btn btn-sm btn-warning" href="'.base_url().'ats/printnote/'.$row['id'].'" target="_blank"><i class="glyphicon glyphicon-print"></i></a>';
                        }, 'field' => 'id'),
            );

            // SQL server connection information
            $sql_details = array(
                'user' => $this->config->item('user'),
                'pass' => $this->config->item('pass'),
                'db'   => $this->config->item('db'),
                'host' => $this->config->item('host')
            );

            $joinQuery = "FROM ".$this->tbnote." AS p, ".$this->tbpns." AS a"; 
            $extraWhere= "p.nip=a.nip";


            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
            );
        }
    }

    public function ajax_detailnote($id)
    {
        $data = $this->M_opd->ajax_getNote($id);
        echo json_encode($data);
    }

    public function printnote($id){

        $data['data'] = $this->M_opd->getNote($id);
        $this->load->view('cetakpenilaian',$data);
        $html = $this->output->get_output();
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("penilaian_".$id.".pdf",array('Attachment'=>0));
          
    }

    public function acc()
    {
        $this->template->opd('template', 'atsacc');
    }

    public function ajax_acc()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            
            //panggil dulu library datatablesnya
            $this->load->library('SSP');
            
            //atur nama tablenya disini
            $table = $this->tbacc;

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(

                array('db' => 'p.id',          'dt' => 'id',          'field' => 'id'),
                array('db' => 'p.nip',          'dt' => 'nip',          'field' => 'nip'),
                array('db' => 'p.nip_atasan',   'dt' => 'nip_atasan',   'field' => 'nip_atasan'),
                array('db' => 'p.blth',         'dt' => 'blth',      'field' => 'blth'),
                array('db' => 'p.status',       'dt' => 'status',       'field' => 'status'),
                array('db' => 'p.tgl_persetujuan',     'dt' => 'tgl_persetujuan',     'field' => 'tgl_persetujuan'),
                array('db' => 'p.jml_kegiatan',    'dt' => 'jml_kegiatan',    'field' => 'jml_kegiatan'),
                array('db' => 'p.sumber_data',       'dt' => 'sumber_data',       'field' => 'sumber_data'),
                
                array( 
                    'db' => 'id', 'dt' => 'aksi',
                    'formatter' => function( $d, $row) {
                        if($this->session->userdata('userid')==$row['nip_atasan'])
                        {
                            return '<a class="btn btn-sm btn-primary" href="'.base_url().'ats/editacc/'.$row['id'].'"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;Edit</a>';
                        }else{
                            return '<a class="btn btn-sm btn-default" href="#"><i class="glyphicon glyphicon-delete"></i>&nbsp;&nbsp;Not Allowed</a>';
                        }
                        }, 'field' => 'id'),
            );

            // SQL server connection information
            $sql_details = array(
                'user' => $this->config->item('user'),
                'pass' => $this->config->item('pass'),
                'db'   => $this->config->item('db'),
                'host' => $this->config->item('host')
            );

            $joinQuery = "FROM ".$this->tbacc." AS p"; 
            $extraWhere= "p.nip_atasan='".$this->session->userdata('userid')."'";


            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
            );
        }
    }

    public function addacc()
    {
        $data['title']  = 'Input Data Persetujuan Atasan';
        $data['id']     = '';
        $data['pns']    = $this->M_opd->getAll();
        $data['nipstaff']    = $this->M_opd->getStaff($this->session->userdata('userid'));
        $data['nipatasan']   = $this->session->userdata('userid');
        $data['nip']   = '';
        $data['blth']   = '';
        $data['status']   = '';
        $data['tanggal']   = '';
        $data['jml']  = '';
        $data['sumber']  = '';
        $this->template->opd('template', 'addacc', $data);
    }

    public function editacc($id)
    {
        $id = $this->uri->segment('3');
        $get = $this->M_opd->getAcc($id);
        foreach ($get->result_array() as $key => $g) {
            $data['id']     = $g['id'];
            $data['nipatasan']   = $g['nip_atasan'];
            $data['nipstaff']    = $this->M_opd->getStaff($g['nip_atasan']);
            $data['nip']         = $g['nip'];
            $data['blth']        = $g['blth'];
            $data['status']      = $g['status'];
            $waktu = $v['tgl_persetujuan'];
            $tgl = substr($waktu, 8,2);
            $bln = substr($waktu, 5,2);
            $thn = substr($waktu, 0,4);
            $jam = substr($waktu, -9);
            $time = $tgl.'-'.$bln.'-'.$thn.$jam;
            $data['tanggal']    = $time;
            $data['jml']         = $g['jml_kegiatan'];
            $data['sumber']      = $g['sumber_data'];
        }
        $data['title']  = 'Update Data Persetujuan Atasan';
        
        $data['pns']    = $this->M_opd->getAll();
        $this->template->opd('template', 'addacc', $data);
    }

    public function saveacc()
    {
        $id         = $this->input->post('id');
        $nipatasan  = $this->input->post('nipatasan');
        $nip        = $this->input->post('nip');
        $blth       = $this->input->post('blth');
        $status     = $this->input->post('status');
        $tanggal    = $this->input->post('tanggal');
        $tgl = substr($tanggal, 0,2);
        $bln = substr($tanggal, 3,2);
        $thn = substr($tanggal, 6,4);
        $ambiltgl = $thn.'-'.$bln.'-'.$tgl;
        $jml        = $this->input->post('jml');
        $sumber     = $this->input->post('sumber');

        if($id>0){
            $thi->M_opd->updaccatasan($id,$nipatasan,$nip,$blth,$status,$ambiltgl,$jml,$sumber);
        }else{
            $thi->M_opd->insaccatasan($id,$nipatasan,$nip,$blth,$status,$ambiltgl,$jml,$sumber);
        }
    }   
}