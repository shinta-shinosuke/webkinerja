<?php 
class Login extends CI_Controller 
{
    function __construct(){
        parent::__construct();
        $this->load->library(array('session','Myauth'));
        $this->clear_cache();
    }

    public function index() 
    { 
        $lvl = $this->session->userdata('userkodeadmin');
        if( $lvl != '')
        {
            redirect('welcome');
        }
        
        $data['iduser'] = '';
        $this->load->view('login',$data);       
    } 

    public function clear_cache()
    {
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
    }

    public function check()
    {

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_user_check');
        $this->form_validation->set_error_delimiters('<p style="color:white"><b>','</b></p>');
        $this->form_validation->set_message('required','%s Tidak boleh kosong');

        if($this->form_validation->run() == FALSE)
        {

            $data['username']   = $this->input->post('username');
            $data['password']   = $this->input->post('password');

            $this->load->view('login',$data);
        }else{
            redirect('welcome');
        }

    }

    public function user_check($pass)
    {

        $username   = $this->input->post('username');
        $table      = 'vlogin';
    

        $cond = array();
        $cond['nip']        = $username;
        $cond['password']   = md5($pass);

        $this->myauth->set_database('default');
        $result = $this->myauth->login($cond,$table);

        if($result == FALSE ){

            $this->form_validation->set_message('user_check','Username atau password Anda salah');
            return FALSE;
        }else{
            $ses = $result->nip;
            $session = array(
                'userid'            => $result['nip'],
                'usernama'          => $result['nama'],
                'useropd'           => $result['opd'],
                'usrsubunitkerja'   => $result['subunitkerja'],
                'userkodeadmin'     => $result['kode_admin'],
                );

            $this->session->set_userdata($session);
            $this->session->set_userdata('logged_in', TRUE); 

            return TRUE;
        }

    }

    public function logout(){
        $this->session->sess_destroy();
        header('location: '.site_url('login'));
    }

} 
?> 
