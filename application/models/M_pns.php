<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_pns extends CI_Model {

    protected $tblibur  = 'libur';
    protected $tbpns    = 'tbl_pns';
    protected $tbskp    = 'skp_tahunan';
    protected $tbquant  = 'kuantitas';
    protected $tbkskp   = 'lap_kinerja_skp';
    protected $tbkprod  = 'lap_kinerja_produktivitas';
    protected $tbnote   = 'penilaian_perilaku';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function save_lap_kprod($id, $tanggal, $nip, $kegiatan, $kuantitas, $satuan, $status)
    {
        $tgl = substr($tanggal, 0,2);
        $bln = substr($tanggal, 3,2);
        $thn = substr($tanggal, 6,4);
        $jam = substr($tanggal, -9);
        $time = $thn.'-'.$bln.'-'.$tgl.$jam;
            $data = array(
                        'tanggal'   => $time,
                        'nip'       => $nip,
                        'kegiatan'  => $kegiatan,
                        'kuantitas' => $kuantitas,
                        'status'    => $status,
                        'satuan'    => $satuan
                    );

            $simpan = $this->db->insert($this->tbkprod, $data);
            if($simpan){
                echo'<script>alert("SUKSES");</script>';
                    redirect('pns/pnskprod');
            }else{
                echo'<script>alert("GAGAL");</script>';
            }
    }

    public function edit_lap_kprod($id, $tanggal, $nip, $kegiatan, $kuantitas, $satuan, $status)
    {
        $tgl = substr($tanggal, 0,2);
        $bln = substr($tanggal, 3,2);
        $thn = substr($tanggal, 6,4);
        $jam = substr($tanggal, -9);
        $time = $thn.'-'.$bln.'-'.$tgl.$jam;
            $data = array(
                        'tanggal'   => $time,
                        'nip'       => $nip,
                        'kegiatan'  => $kegiatan,
                        'kuantitas' => $kuantitas,
                        'status'    => $status,
                        'satuan'    => $satuan
                    );

            $this->db->where('id_lap_prod', $id);
            $simpan = $this->db->update($this->tbkprod, $data);
            if($simpan){
                echo'<script>alert("SUKSES");</script>';
                    redirect('pns/pnskprod');
            }else{
                echo'<script>alert("GAGAL");</script>';
            }
    }

        public function save_lap_skp($id, $tanggal, $nip, $id_skp, $kegiatan, $kuantitas, $satuan, $status, $sesuai)
    {
        $tgl = substr($tanggal, 0,2);
        $bln = substr($tanggal, 3,2);
        $thn = substr($tanggal, 6,4);
        $jam = substr($tanggal, -9);
        $time = $thn.'-'.$bln.'-'.$tgl.$jam;
            $data = array(
                        'tanggal'   => $time,
                        'nip'       => $nip,
                        'id_skp_tahunan'     => $id_skp,
                        'kegiatan'  => $kegiatan,
                        'kuantitas' => $kuantitas,
                        'status'    => $status,
                        'satuan'    => $satuan,
                        'sesuai'    => $sesuai
                    );

            $simpan = $this->db->insert($this->tbkskp, $data);
            if($simpan){
                echo'<script>alert("SUKSES");</script>';
                    redirect('pns/pnskskp');
            }else{
                echo'<script>alert("GAGAL");</script>';
            }
    }

    public function edit_lap_skp($id, $tanggal, $nip, $id_skp, $kegiatan, $kuantitas, $satuan, $status, $sesuai)
    {
        $tgl = substr($tanggal, 0,2);
        $bln = substr($tanggal, 3,2);
        $thn = substr($tanggal, 6,4);
        $jam = substr($tanggal, -9);
        $time = $thn.'-'.$bln.'-'.$tgl.$jam;

            $data = array(
                        'tanggal'   => $time,
                        'nip'       => $nip,
                        'id_skp_tahunan'     => $id_skp,
                        'kegiatan'  => $kegiatan,
                        'kuantitas' => $kuantitas,
                        'satuan'    => $satuan,
                        'sesuai'    => $sesuai
                    );

            $this->db->where('id_lap_skp', $id);
            $simpan = $this->db->update($this->tbkskp, $data);
            if($simpan){
                echo'<script>alert("SUKSES");</script>';
                    redirect('pns/pnskskp');
            }else{
                echo'<script>alert("GAGAL");</script>';
            }
    }

    public function saveSkp($id, $nip, $tahun, $kegiatan, $targetkuantitas, $satuankuantitas, $targetpenyelesaian)
    {
        $data = array(
                    'nip'       => $nip,
                    'tahun'     => $tahun,
                    'kegiatan'  => $kegiatan,
                    'target_kuantitas' => $targetkuantitas,
                    'satuan_kuantitas' => $satuankuantitas,
                    'target_selesai' => $targetpenyelesaian
                );

        $simpan = $this->db->insert($this->tbskp, $data);
        if($simpan){
            echo'<script>alert("SUKSES");</script>';
                redirect('pns/pnsskp');
        }else{
            echo'<script>alert("GAGAL");</script>';
        }
    }

    public function editSkp($id, $nip, $tahun, $kegiatan, $targetkuantitas, $satuankuantitas, $targetpenyelesaian)
    {
        $data = array(
                    'nip'       => $nip,
                    'tahun'     => $tahun,
                    'kegiatan'  => $kegiatan,
                    'target_kuantitas' => $targetkuantitas,
                    'satuan_kuantitas' => $satuankuantitas,
                    'target_selesai' => $targetpenyelesaian
                );

        $this->db->where('id_skp', $id);
        $simpan = $this->db->update($this->tbskp, $data);
        if($simpan){
            echo'<script>alert("SUKSES");</script>';
                redirect('pns/pnsskp');
        }else{
            echo'<script>alert("GAGAL");</script>';
        }
    }
}
