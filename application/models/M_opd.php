<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_opd extends CI_Model {

    protected $tblogin  = 'vlogin';
    protected $tblibur  = 'libur';
    protected $tbloker  = 'tab_loker';
    protected $tbpns    = 'tbl_pns';
    protected $tbpnsxt  = 'tbl_pns_ext';
    protected $tbskp    = 'skp_tahunan';
    protected $tbquant  = 'kuantitas';
    protected $tbkskp   = 'lap_kinerja_skp';
    protected $tbkprod  = 'lap_kinerja_produktivitas';
    protected $tbnote   = 'penilaian_perilaku';
    protected $tbacc   = 'persetujuan_atasan';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function cekPasswd($nip)
    {
        $this->db->where('nip', $nip);
        $que = $this->db->get($this->tbpnsxt);

        return $que;
    }

    public function savePasswd($nip, $password)
    {
        $data = array(
                    'nip'       => $nip,
                    'password'  => md5($password),
                    'repassword'  => $password
                );

        $this->db->where('nip', $nip);
        $simpan = $this->db->update($this->tbpnsxt, $data);
        if($simpan){
            echo'<script>alert("SUKSES");</script>';
                redirect('welcome');
        }else{
            echo'<script>alert("GAGAL");</script>';
        }
    }

    public function cek_atasan($id)
    {
        $this->db->select('*, substr(loker,1,2) as loker');
        $this->db->from($this->tbpnsxt.' a');
        $this->db->join($this->tbpns.' b', 'a.nip = b.nip');
        $this->db->where('a.id', $id);
        $que = $this->db->get($this->tbpnsxt);

        return $que;
    }

    public function getQuant()
    {
        $que = $this->db->get($this->tbquant);

        return $que;
    }

    public function getStaff($id)
    {
        $query = $this->db->query("select a.nip,a.nama from tbl_pns a where a.nip IN (SELECT nip FROM tbl_pns_ext WHERE nip_atasan1='".$id."' OR nip_atasan2='".$id."' OR nip_atasan3='".$id."')");
        return $query;
    }

    public function getBeforeAtasan()
    {
        $query = $this->db->query("select a.nip,a.nama from tbl_pns a where a.nip NOT IN (select nip from tbl_pns_ext)");
        return $query;
    }

    public function getStaffOpd()
    {
        $this->db->select('nip,nama');
        $this->db->where('opd',$this->session->userdata('useropd'));
        $que = $this->db->get($this->tblogin);

        return $que;
    }

    public function getAll()
    {
        $this->db->select('nip,nama');
        $que = $this->db->get($this->tbpns);

        return $que;
    }

    public function getAllLoker()
    {
        $que = $this->db->get($this->tbloker);

        return $que;
    }

    public function getById($id)
    {
        $this->db->where('id_skp', $id);
        $que = $this->db->get($this->tbskp);

        return $que;
    }

    public function saveSkp($url, $id, $nip, $tahun, $kegiatan, $targetkuantitas, $satuankuantitas, $targetpenyelesaian)
    {
        $data = array(
                    'nip'       => $nip,
                    'tahun'     => $tahun,
                    'kegiatan'  => $kegiatan,
                    'target_kuantitas' => $targetkuantitas,
                    'satuan_kuantitas' => $satuankuantitas,
                    'target_selesai' => $targetpenyelesaian
                );

        $simpan = $this->db->insert($this->tbskp, $data);
        if($simpan){
            echo'<script>alert("SUKSES");</script>';
                redirect($url.'/skp');
        }else{
            echo'<script>alert("GAGAL");</script>';
        }
    }

    public function editSkp($url, $id, $nip, $tahun, $kegiatan, $targetkuantitas, $satuankuantitas, $targetpenyelesaian)
    {
        $data = array(
                    'nip'       => $nip,
                    'tahun'     => $tahun,
                    'kegiatan'  => $kegiatan,
                    'target_kuantitas' => $targetkuantitas,
                    'satuan_kuantitas' => $satuankuantitas,
                    'target_selesai' => $targetpenyelesaian
                );

        $this->db->where('id_skp', $id);
        $simpan = $this->db->update($this->tbskp, $data);
        if($simpan){
            echo'<script>alert("SUKSES");</script>';
                redirect($url.'/skp');
        }else{
            echo'<script>alert("GAGAL");</script>';
        }
    }
 
    public function simpanpns($id,$nip,$nama,$gelard,$gelarb,$golongan,$eselon,$jabatan,$jenisjabatan)
    {
        $data = array(
                'nip'           => $nip,
                'nama'          => $nama,
                'gelar_d'       => $gelard,
                'gelar_b'       => $gelarb,
                'kode_gol'      => $golongan,
                'kode_eselon'   => $eselon,
                'ked_pegawai'   => '1', //ini field buat apa dan nilainya apa saja
                'jenis_jab'     => $jenisjabatan,
                'id_jab'        => '00', //ini field ketentuannya bagaimana
                'nama_jab'      => $jabatan, //ini field list nya apa saja
                'loker'         => '0', //ini field buat apa dan nilainya apa saja
                );

        if($id>0){
            $this->db->where('id',$id);
            $simpan = $this->db->update($this->tbpns, $data);
        }else{
            $simpan = $this->db->insert($this->tbpns, $data);
        }

        if($simpan){
            echo'<script>alert("SUKSES");</script>';
                redirect('dashboard/pns');
        }else{
            echo'<script>alert("GAGAL");</script>';
        }
    }

    public function ajax_getpns($id)
    {
        $this->db->where('id',$id);
        $query = $this->db->get($this->tbpns);
 
        return $query->row();
    }

    public function ceklibur($tgl)
    {
        $this->db->where('tanggal',$tgl);
        $query = $this->db->get($this->tblibur);
 
        return $query;
    }

    public function save_lap_skp($url, $id, $tanggal, $nip, $id_skp, $kegiatan, $kuantitas, $satuan, $status, $sesuai)
    {
        $tgl = substr($tanggal, 0,2);
        $bln = substr($tanggal, 3,2);
        $thn = substr($tanggal, 6,4);
        $jam = substr($tanggal, -9);
        $time = $thn.'-'.$bln.'-'.$tgl.$jam;
            $data = array(
                        'tanggal'   => $time,
                        'nip'       => $nip,
                        'id_skp_tahunan'     => $id_skp,
                        'kegiatan'  => $kegiatan,
                        'kuantitas' => $kuantitas,
                        'status'    => $status,
                        'satuan'    => $satuan,
                        'sesuai'    => $sesuai
                    );

            $simpan = $this->db->insert($this->tbkskp, $data);
            if($simpan){
                echo'<script>alert("SUKSES");</script>';
                    redirect($url.'/kskp');
            }else{
                echo'<script>alert("GAGAL");</script>';
            }
    }

    public function edit_lap_skp($url, $id, $tanggal, $nip, $id_skp, $kegiatan, $kuantitas, $satuan, $status, $sesuai)
    {
        $tgl = substr($tanggal, 0,2);
        $bln = substr($tanggal, 3,2);
        $thn = substr($tanggal, 6,4);
        $jam = substr($tanggal, -9);
        $time = $thn.'-'.$bln.'-'.$tgl.$jam;

            $data = array(
                        'tanggal'   => $time,
                        'nip'       => $nip,
                        'id_skp_tahunan'     => $id_skp,
                        'kegiatan'  => $kegiatan,
                        'kuantitas' => $kuantitas,
                        'satuan'    => $satuan,
                        'status'    => $status,
                        'sesuai'    => $sesuai
                    );

            $this->db->where('id_lap_skp', $id);
            $simpan = $this->db->update($this->tbkskp, $data);
            if($simpan){
                echo'<script>alert("SUKSES");</script>';
                    redirect($url.'/kskp');
            }else{
                echo'<script>alert("GAGAL");</script>';
            }
    }

    public function cek_lap_kinerja($id)
    {
        $this->db->where("nip", $id);
        $res = $this->db->get($this->tbkskp);
        return $res->num_rows();
    }

    public function getKskpById($id)
    {
        $res = $this->db->query("SELECT c.id_lap_skp,c.tanggal,a.nip,a.id_skp,b.id as numsatuan, b.kuantitas as satuan, c.kuantitas as tercapai, c.kegiatan as pekerjaan, c.status, c.sesuai FROM skp_tahunan a, kuantitas b, lap_kinerja_skp c where a.satuan_kuantitas=b.id AND a.id_skp=c.id_skp_tahunan AND c.id_lap_skp='$id'");
        return $res;
    }

    public function getskptahunan($nip)
    {
        $this->db->where("nip", $nip);
        $res = $this->db->get($this->tbskp);
        return $res;
    }

    public function getKprodById($id)
    {
        $this->db->select('*, a.kuantitas as qty');
        $this->db->from($this->tbkprod.' a');
        $this->db->join($this->tbquant.' b', 'a.satuan = b.id');
        $res = $this->db->get();
        return $res;
    }

    public function save_lap_kprod($url, $id, $tanggal, $nip, $kegiatan, $kuantitas, $satuan, $status)
    {
        $tgl = substr($tanggal, 0,2);
        $bln = substr($tanggal, 3,2);
        $thn = substr($tanggal, 6,4);
        $jam = substr($tanggal, -9);
        $time = $thn.'-'.$bln.'-'.$tgl.$jam;
            $data = array(
                        'tanggal'   => $time,
                        'nip'       => $nip,
                        'kegiatan'  => $kegiatan,
                        'kuantitas' => $kuantitas,
                        'status'    => $status,
                        'satuan'    => $satuan
                    );

            $simpan = $this->db->insert($this->tbkprod, $data);
            if($simpan){
                echo'<script>alert("SUKSES");</script>';
                    redirect($url.'/kprod');
            }else{
                echo'<script>alert("GAGAL");</script>';
            }
    }

    public function edit_lap_kprod($url, $id, $tanggal, $nip, $kegiatan, $kuantitas, $satuan, $status)
    {
        $tgl = substr($tanggal, 0,2);
        $bln = substr($tanggal, 3,2);
        $thn = substr($tanggal, 6,4);
        $jam = substr($tanggal, -9);
        $time = $thn.'-'.$bln.'-'.$tgl.$jam;
            $data = array(
                        'tanggal'   => $time,
                        'nip'       => $nip,
                        'kegiatan'  => $kegiatan,
                        'kuantitas' => $kuantitas,
                        'status'    => $status,
                        'satuan'    => $satuan
                    );

            $this->db->where('id_lap_prod', $id);
            $simpan = $this->db->update($this->tbkprod, $data);
            if($simpan){
                echo'<script>alert("SUKSES");</script>';
                    redirect($url.'/kprod');
            }else{
                echo'<script>alert("GAGAL");</script>';
            }
    }

    public function inssetting($id, $keterangan, $tanggal)
    {
        $tgl = substr($tanggal, 0,2);
        $bln = substr($tanggal, 3,2);
        $thn = substr($tanggal, 6,4);
        $time = $thn.'-'.$bln.'-'.$tgl;

        $data = array(
                    'tanggal'   => $time,
                    'keterangan'    => $keterangan
                );

        $simpan = $this->db->insert($this->tblibur, $data);
        if($simpan){
            echo'<script>alert("SUKSES");</script>';
                redirect('opd/setting');
        }else{
            echo'<script>alert("GAGAL");</script>';
        }
    }

    public function updsetting($id, $keterangan, $tanggal)
    {
        $tgl = substr($tanggal, 0,2);
        $bln = substr($tanggal, 3,2);
        $thn = substr($tanggal, 6,4);
        $time = $thn.'-'.$bln.'-'.$tgl;

        $data = array(
                    'tanggal'   => $ime,
                    'keterangan'    => $keterangan
                );

        $this->db->where('idlibur', $id);
        $simpan = $this->db->update($this->tblibur, $data);
        if($simpan){
            echo'<script>alert("SUKSES");</script>';
                redirect('opd/setting');
        }else{
            echo'<script>alert("GAGAL");</script>';
        }
    }

    public function getlibur($id)
    {
        $this->db->where('idlibur', $id);
        $res = $this->db->get($this->tblibur);
        return $res;
    }

    public function insnote($id, $nip, $nip_atasan, $blth, $pelayanan, $integritas, $komitmen, $disiplin, $kerjasama, $kepemimpinan, $tanggal, $tgl_edit)
    {
        /*$tgl = substr($tgl_edit, 0,2);
        $bln = substr($tgl_edit, 3,2);
        $thn = substr($tgl_edit, 6,4);
        $jam = substr($tgl_edit, -9);
        $time = $thn.'-'.$bln.'-'.$tgl.$jam;*/
            $data = array(
                        'tgl_pembuatan'   => $tanggal,
                        'tgl_edit'        => $tgl_edit,
                        'nip'             => $nip,
                        'nip_atasan'      => $nip_atasan,
                        'blth'            => $blth,
                        'pelayanan'       => $pelayanan,
                        'integritas'      => $integritas,
                        'komitmen'        => $komitmen,
                        'disiplin'        => $disiplin,
                        'kerjasama'       => $kerjasama,
                        'kepemimpinan'    => $kepemimpinan,
                    );

            $simpan = $this->db->insert($this->tbnote, $data);
            if($simpan){
                echo'<script>alert("SUKSES");</script>';
                    redirect('ats/note');
            }else{
                echo'<script>alert("GAGAL");</script>';
            }        
    }

    public function updnote($id, $nip, $nip_atasan, $blth, $pelayanan, $integritas, $komitmen, $disiplin, $kerjasama, $kepemimpinan, $tanggal, $tgl_edit)
    {
            $data = array(
                        'tgl_edit'        => $tgl_edit,
                        'nip'             => $nip,
                        'nip_atasan'      => $nip_atasan,
                        'blth'            => $blth,
                        'pelayanan'       => $pelayanan,
                        'integritas'      => $integritas,
                        'komitmen'        => $komitmen,
                        'disiplin'        => $disiplin,
                        'kerjasama'       => $kerjasama,
                        'kepemimpinan'    => $kepemimpinan,
                    );

            $this->db->where('id',$id);
            $simpan = $this->db->update($this->tbnote, $data);
            if($simpan){
                echo'<script>alert("SUKSES");</script>';
                    redirect('ats/note');
            }else{
                echo'<script>alert("GAGAL");</script>';
            }        
    }

    public function getNote($id)
    {
        $this->db->where('id', $id);
        $res = $this->db->get($this->tbnote);
        return $res;
    }

    public function ajax_getNote($id)
    {
        
        $this->db->where('id',$id);
        $query = $this->db->get($this->tbnote);
 
        return $query->row();
    }

    public function insatasan($id, $nip, $nip1, $nip2, $nip3, $unit, $password, $kodeadmin)
    {

        $data = array(
                    'nip'   => $nip,
                    'nip_atasan1'   => $nip1,
                    'nip_atasan2'   => $nip2,
                    'nip_atasan3'   => $nip3,
                    'kode_admin'   => $kodeadmin,
                );

        $simpan = $this->db->insert($this->tbpnsxt, $data);
        if($simpan){
            echo'<script>alert("SUKSES");</script>';
                redirect('dashboard/atasan');
        }else{
            echo'<script>alert("GAGAL");</script>';
        }
    }

    public function updatasan($id, $nip, $nip1, $nip2, $nip3, $unit, $kodeadmin)
    {
        $data = array(
                    'nip'   => $nip,
                    'nip_atasan1'   => $nip1,
                    'nip_atasan2'   => $nip2,
                    'nip_atasan3'   => $nip3,
                    'kode_admin'   => $kodeadmin,
                );

        $this->db->where('nip', $nip);
        $simpan = $this->db->update($this->tbpnsxt, $data);
        if($simpan){
            echo'<script>alert("SUKSES");</script>';
                redirect('dashboard/atasan');
        }else{
            echo'<script>alert("GAGAL");</script>';
        }
    }

    public function updatasanpswd($id, $nip, $nip1, $nip2, $nip3, $unit, $password, $kodeadmin)
    {

        $data = array(
                    'nip'   => $nip,
                    'nip_atasan1'   => $nip1,
                    'nip_atasan2'   => $nip2,
                    'nip_atasan3'   => $nip3,
                    'password'      => md5($password),
                    'repassword'      => $password,
                    'kode_admin'   => $kodeadmin,
                );

        $this->db->where('nip', $nip);
        $simpan = $this->db->update($this->tbpnsxt, $data);
        if($simpan){
            echo'<script>alert("SUKSES");</script>';
                redirect('dashboard/atasan');
        }else{
            echo'<script>alert("GAGAL");</script>';
        }
    }

    public function insaccatasan($id,$nipatasan,$nip,$blth,$status,$ambiltgl,$jml,$sumber)
    {
        $data = array(
                    'nip'   => $nip,
                    'nip_atasan'   => $nipatasan,
                    'blth' => $blth,
                    'status' => $status,
                    'tgl_persetujuan' => $ambiltgl,
                    'jml_kegiatan' => $jml,
                    'sumber_data' => $sumber

                );

        $simpan = $this->db->insert($this->tbacc, $data);
        if($simpan){
            echo'<script>alert("SUKSES");</script>';
                redirect('ats/acc');
        }else{
            echo'<script>alert("GAGAL");</script>';
        }
    }

    public function updaccatasan($id,$nipatasan,$nip,$blth,$status,$ambiltgl,$jml,$sumber)
    {
        $data = array(
                    'nip'   => $nip,
                    'nip_atasan'   => $nipatasan,
                    'blth' => $blth,
                    'status' => $status,
                    'tgl_persetujuan' => $ambiltgl,
                    'jml_kegiatan' => $jml,
                    'sumber_data' => $sumber

                );

        $this->db->where('id', $id);
        $simpan = $this->db->update($this->tbacc, $data);
        if($simpan){
            echo'<script>alert("SUKSES");</script>';
                redirect('ats/acc');
        }else{
            echo'<script>alert("GAGAL");</script>';
        }
    }
}
