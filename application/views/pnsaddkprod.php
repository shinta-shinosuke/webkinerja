            <div class="container-fluid">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo $title; ?></div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="<?php echo base_url()?>pns/savekprod" method="post">
                            <input type="hidden" name="id" class="form-control" value="<?php echo $id; ?>" />
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
                                <div class="col-sm-3">
                                    <input type="text" name="tanggal" placeholder="Tanggal" class="form-control" id="datetimepicker1" value="<?php echo $tanggal; ?>" required />
                                    <span style="color:red"><b><?php echo $error;?></b></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">NIP</label>
                                <div class="col-sm-6">
                                    <select name="nip" id="nip" class="form-control" readonly >
                                            <option value="">-- Pilih Disini --</option>
                                            <?php
                                                foreach ($pns->result_array() as $key => $v) {
                                            ?>
                                            <option value="<?php echo $v['nip'];?>" <?php if($v['nip']==$nip){ echo " selected"; } ?>><?php echo $v['nip']." - ".$v['nama']; ?></option>
                                            <?php
                                                }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Kegiatan</label>
                                <div class="col-sm-6">
                                <textarea name="kegiatan" id="kegiatan" class="form-control" required><?php echo $kegiatan; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Kuantitas</label>
                                <div class="col-sm-2">
                                    <input type="text" name="kuantitas" id="kuantitas" class="form-control" value="<?php echo $kuantitas; ?>"  required />
                                </div>
                                <div class="col-sm-7">
                                    <div class="col-sm-7">
                                        <select name="satuankuantitas" class="form-control" required>
                                            <option value="">-- Pilih Disini --</option>
                                            <?php
                                                foreach ($satkuantitas->result_array() as $key => $s) {
                                            ?>
                                                <option value="<?php echo $s['id'];?>" <?php if($s['id']==$satuankuantitas){ echo " selected"; } ?>><?php echo $s['kuantitas']; ?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-2">
                                    <input type="text" name="status" value="<?php echo $status; ?>" class="form-control" required />
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group text-right" style="margin-top:20px">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?php echo base_url();?>pns/pnskprod"><button type="button" class="btn btn-warning">Batal</button></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            