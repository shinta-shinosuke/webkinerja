<style type="text/css">
              table{
                      border-collapse: collapse;
                      font-family: arial;
                      width: 100%;
                      padding: 5px;
                      font-size: 12pt;
                    }
              td{
                      border-top: 0.5px solid #e3e3e3;
                      padding: 3px;
                      vertical-align: top;
                    }
              th{
                      border-top: 0.5px solid #e3e3e3;
                      border-bottom: 0.5px solid #e3e3e3;
                      border-left: 0.5px solid #e3e3e3;
                      border-right: 0.5px solid #e3e3e3;
                      padding: 3px;
                    }

              table tr:first-child td {
                border-top: 0;
              }
              table tr:last-child td {
                border-bottom: 0;
              }
              table tr td:first-child,
              table tr th:first-child {
                border-left: 0;
              }
              table tr td:last-child,
              table tr th:last-child {
                border-right: 0;
              }
                </style>
                <?php foreach ($data->result_array() as $c) {}?>
                <img src="<?php echo base_url();?>Admin/images/logo.png" alt="Smiley face">
                <center><h3>PENILAIAN PERILAKU</h3></center>
                <hr>
                <table width="50%" align='left'>
                  <tr>
                      <td>BLTH</td>
                      <td>:</td>
                      <td><?php echo $c['blth'];?></td>
                  </tr>
                  <tr>
                      <td>NIP</td>
                      <td>:</td>
                      <td><?php echo $c['nip'];?></td>
                  </tr>
                  <tr>
                      <td>Pelayanan</td>
                      <td>:</td>
                      <td><?php echo $c['pelayanan'];?></td>
                  </tr>
                  <tr>
                      <td>Integritas</td>
                      <td>:</td>
                      <td><?php echo $c['integritas'];?></td>
                  </tr>
                  <tr>
                      <td>Komitmen</td>
                      <td>:</td>
                      <td><?php echo $c['komitmen'];?></td>
                  </tr>
                  <tr>
                      <td>Disiplin</td>
                      <td>:</td>
                      <td><?php echo $c['disiplin'];?></td>
                  </tr>
                  <tr>
                      <td>Kerjasama</td>
                      <td>:</td>
                      <td><?php echo $c['kerjasama'];?></td>
                  </tr>
                  <tr>
                      <td>Kepemimpinan</td>
                      <td>:</td>
                      <td><?php echo $c['kepemimpinan'];?></td>
                  </tr>
                </table>