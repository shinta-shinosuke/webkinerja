<div class="container-fluid">
                <div class="panel panel-default">
                    <div class="panel-heading">Data Catatan Perilaku</div>
                    <div class="panel-body">
                        <table class="table table-bordered table-hover" id="mytable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Aksi</th>
                                    <th>NIP</th>
                                    <th>NIP Atasan</th>
                                    <th>BLTH</th>
                                    <th>Tanggal</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        <script src="<?php echo base_url();?>Admin/plugins/jQuery/jQuery-2.2.0.min.js"></script>
        <script src="<?php echo base_url('Admin/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('Admin/datatables/dataTables.bootstrap.js') ?>"></script>
        <?php $url=site_url('opd/ajax_pnsnote');?>
        <?php $urldetail=site_url('opd/ajax_detailnote');?>
        <script type="text/javascript">

            $(document).ready(function () {
                $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                var t = $('#mytable').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "searching" : false,
                    "ajax": '<?php echo $url; ?>',
                    "columns": [
                        {
                            "data": null,
                            "class": "text-center",
                            "orderable": false
                        },
                            {
                            "class": "text-center",
                            "data": "aksi"
                        },
                        {"data": "nip"},
                        //{"data": "nama"},
                        {"data": "nip_atasan"},
                        {"data": "blth"},
                        {"data": "tgl_pembuatan"}
                    
                    ],
                    "order": [[1, 'desc']],
                    "rowCallback": function (row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
            });

function detail_note(id)
{
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Detail Data Penilaian Perilaku'); // Set Title to Bootstrap modal title
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo $urldetail; ?>/"+id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="nip"]').val(data.nip);
            $('[name="nip_atasan"]').val(data.nip_atasan);
            $('[name="blth"]').val(data.blth);
            $('[name="pelayanan"]').val(data.pelayanan);
            $('[name="integritas"]').val(data.integritas);
            $('[name="komitmen"]').val(data.komitmen);
            $('[name="disiplin"]').val(data.disiplin);
            $('[name="kerjasama"]').val(data.kerjasama);
            $('[name="kepemimpinan"]').val(data.kepemimpinan);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
        </script>


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <div class="modal-body form">
                <form action="" id="form" name="form" class="form-horizontal" role="form" method="POST">
                    <input type="hidden" name="id"/>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">NIP Staff</label>
                            <div class="col-md-9">
                                <input type="text" name="nip" placeholder="Nomor Induk Pegawai Staf" class="form-control" readonly />
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">NIP Atasan</label>
                            <div class="col-md-9">
                                <input type="text" name="nip_atasan" placeholder="Nomor Induk Pegawai Atasan" class="form-control" readonly />
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                                <label class="control-label col-md-3">Pelayanan</label>
                                <div class="col-md-9">
                                    <input type="text" name="pelayanan" id="pelayanan" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Integritas</label>
                                <div class="col-md-9">
                                    <input type="text" name="integritas" id="integritas" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Komitmen</label>
                                <div class="col-md-9">
                                    <input type="text" name="komitmen" id="komitmen" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Disiplin</label>
                                <div class="col-md-9">
                                    <input type="text" name="disiplin" id="disiplin" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Kerjasama</label>
                                <div class="col-md-9">
                                    <input type="text" name="kerjasama" id="kerjasama" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Kepemimpinan</label>
                                <div class="col-md-9">
                                    <input type="text" name="kepemimpinan" id="kepemimpinan" class="form-control" readonly />
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->