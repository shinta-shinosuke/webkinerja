    <!-- Content Header (Page header) -->
    <section class="content-header" style="margin-left:280px">
      <h1>
        404 Error Page
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">404 error</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content"  style="margin-left:280px">
      <div class="error-page">
        <h2 class="headline text-yellow"> 404</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-yellow"></i> Oops! Halaman yang diminta tidak tersedia. Hubungi Administrator</h3>

          <p>
            <a href="<?php echo base_url(); ?>">Kembali ke Halaman Utama.</a>
          </p>
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>
    <!-- /.content -->