            <div class="container-fluid">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo $title; ?></div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="<?php echo site_url('opd/savesetting');?>" method="post">
                            <input type="hidden" name="id" class="form-control" value="<?php echo $id; ?>" />
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
                                <div class="col-sm-3">
                                    <input type="text" name="tanggal" placeholder="Tanggal" class="form-control" id="tanggal" value="<?php echo $tanggal; ?>" required />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Keterangan</label>
                                <div class="col-sm-6">
                                    <input type="text" name="keterangan" class="form-control" value="<?php echo $keterangan; ?>" />
                                </div>
                            </div>
                            <div class="form-group text-right" style="margin-top:20px">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?php echo base_url();?>opd/setting"><button type="button" class="btn btn-warning">Batal</button></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            