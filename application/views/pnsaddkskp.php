            <div class="container-fluid">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo $title; ?></div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="<?php echo base_url()?>pns/savekskp" method="post">
                            <input type="hidden" name="id" class="form-control" value="<?php echo $id; ?>" />
                            <input type="hidden" id="numsatuan" name="numsatuan" class="form-control" />
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
                                <div class="col-sm-3">
                                    <input type="text" name="tanggal" placeholder="Tanggal" class="form-control" id="datetimepicker1" value="<?php echo $tanggal; ?>" required />
                                    <span style="color:red"><b><?php echo $error;?></b></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">NIP</label>
                                <div class="col-sm-6">
                                    <select name="nip" id="nip" class="form-control" readonly >
                                            <option value="">-- Pilih Disini --</option>
                                            <?php
                                                foreach ($pns->result_array() as $key => $v) {
                                            ?>
                                            <option value="<?php echo $v['nip'];?>" <?php if($v['nip']==$nip){ echo " selected"; } ?>><?php echo $v['nip']." - ".$v['nama']; ?></option>
                                            <?php
                                                }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">SKP Tahunan</label>
                                <div class="col-sm-6">
                                    <select name="skp" id="skp" class="form-control" required>
                                        <option value="">-- Pilih Disini --</option>
                                            <?php
                                                foreach ($getskp->result_array() as $key => $s) {
                                            ?>
                                            <option value="<?php echo $s['id_skp'];?>" <?php if($s['id_skp']==$skp){ echo " selected"; } ?>><?php echo $s['kegiatan']; ?></option>
                                            <?php
                                                }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Kegiatan</label>
                                <div class="col-sm-6">
                                <textarea name="kegiatan" id="kegiatan" class="form-control" required><?php echo $kegiatan; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Kuantitas</label>
                                <div class="col-sm-2">
                                    <input type="text" name="kuantitas" id="kuantitas" class="form-control" value="<?php echo $kuantitas; ?>"  required />
                                    <!--<select name="kuantitas" class="form-control" required>
                                        <option value="">-- Pilih Disini --</option>
                                        <?php
                                            /*$max = $targetkuantitas;
                                            for ($x = $max; $x >= 1; $x--) {
                                        <option value="<?php echo $x;?>" <?php if($x==$max){ echo " selected"; } ?>><?php echo $x; ?></option>
                                            }*/
                                        ?>
                                    </select>-->
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" id="satuan" name="satuan" value="<?php echo $satuan; ?>" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-2">
                                    <input type="text" name="status" value="<?php echo $status; ?>" class="form-control" required />
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Sesuai</label>
                                <div class="col-sm-2">
                                    <input type="text" name="sesuai" value="<?php echo $sesuai; ?>" class="form-control" required />
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group text-right" style="margin-top:20px">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?php echo base_url();?>pns/pnskskp"><button type="button" class="btn btn-warning">Batal</button></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            