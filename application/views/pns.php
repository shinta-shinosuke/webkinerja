<div class="container-fluid">
                <div class="panel panel-default">
                    <div class="panel-heading">Data PNS</div>
                    <div class="panel-body">
                        <a class="btn btn-sm btn-success" href="<?php echo base_url();?>dashboard/addpns"><i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;Tambah Data</a>
                        <hr/>
                        <table class="table table-bordered table-hover" id="mytable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Aksi</th>
                                    <th>NIP</th>
                                    <th>Nama</th>
                                    <th>Golongan</th>
                                    <th>Eselon</th>
                                    <th>Jabatan</th>
                                    <th>Jenis Jabatan</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        <script src="<?php echo base_url();?>Admin/plugins/jQuery/jQuery-2.2.0.min.js"></script>
        <script src="<?php echo base_url('Admin/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('Admin/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">

            $(document).ready(function () {
                $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                var t = $('#mytable').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "searching" : false,
                    "ajax": '<?php echo site_url('dashboard/ajax_pns'); ?>',
                    "columns": [
                        {
                            "data": null,
                            "class": "text-center",
                            "orderable": false
                        },
                            {
                            "class": "text-center",
                            "data": "aksi"
                        },
                        {"data": "nip"},
                        {"data": "nama"},
                        {"data": "golongan"},
                        {"data": "eselon"},
                        {"data": "jabatan"},
                        {"data": "jenisjabatan"},
                    
                    ],
                    "order": [[1, 'desc']],
                    "rowCallback": function (row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
            });

function add_pns()
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Tambah Data PNS'); // Set Title to Bootstrap modal title
}

function edit_pns(idpns)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Edit Data PNS'); // Set Title to Bootstrap modal title
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('dashboard/ajax_editpns'); ?>/"+idpns,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="nip"]').val(data.nip);
            $('[name="nama"]').val(data.nama);
            $('[name="gelard"]').val(data.gelar_d);
            $('[name="gelarb"]').val(data.gelar_b);
            $('[name="golongan"]').val(data.kode_gol);
            $('[name="eselon"]').val(data.kode_eselon);
            $('[name="jenisjab"]').val(data.jenis_jab);
            $('[name="jabatan"]').val(data.nama_jab);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <div class="modal-body form">
                <form action="<?php echo site_url('dashboard/ajax_addpns');?>" id="form" name="form" class="form-horizontal" role="form" method="POST">
                    <input type="hidden" name="id"/>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">NIP</label>
                            <div class="col-md-9">
                                <input type="text" name="nip" placeholder="Nomor Induk Pegawai" class="form-control" required />
                                <span class="help-block">Harap isi bidang ini dengan angka</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Lengkap</label>
                            <div class="col-md-9">
                                <input type="text" name="nama" id="nama" placeholder="Nama Lengkap" class="form-control" required />
                                <span id="nameError" class="help-block">Harap isi bidang ini dengan huruf</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Gelar Depan</label>
                            <div class="col-md-9">
                                <input type="text" name="gelard" id="gelard" placeholder="Gelar Depan" class="form-control" required />
                                <span id="nameError" class="help-block">Harap isi bidang ini dengan huruf</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Gelar Belakang</label>
                            <div class="col-md-9">
                                <input type="text" name="gelarb" id="gelarb" placeholder="Gelar Belakang" class="form-control" required />
                                <span id="nameError" class="help-block">Harap isi bidang ini dengan huruf</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Golongan</label>
                            <div class="col-md-9">
                                <select name="golongan" class="form-control" required>
                                    <option value="">--Pilih Disini--</option>
                                    <option value="11">I/a</option>
                                    <option value="12">I/b</option>
                                    <option value="13">I/c</option>
                                    <option value="14">I/d</option>
                                    <option value="21">II/a</option>
                                    <option value="22">II/b</option>
                                    <option value="23">II/c</option>
                                    <option value="24">II/d</option>
                                    <option value="31">III/a</option>
                                    <option value="32">III/b</option>
                                    <option value="33">III/c</option>
                                    <option value="34">III/d</option>
                                    <option value="41">IV/a</option>
                                    <option value="42">IV/b</option>
                                    <option value="43">IV/c</option>
                                    <option value="44">IV/d</option>
                                    <option value="45">IVe</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Eselon</label>
                            <div class="col-md-9">
                                <select name="eselon" class="form-control" required>
                                    <option value="">--Pilih Disini--</option>
                                    <option value="11">I.a</option>
                                    <option value="12">I.b</option>
                                    <option value="21">2.a</option>
                                    <option value="22">2.b</option>
                                    <option value="31">3.a</option>
                                    <option value="32">3.b</option>
                                    <option value="41">4.a</option>
                                    <option value="42">4.b</option>
                                    <option value="51">5.a</option>
                                    <option value="52">5.b</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Jabatan</label>
                            <div class="col-md-9">
                                <select name="jabatan" class="form-control" required>
                                    <option value="">--Pilih Disini--</option>
                                    <option value="staff">STAFF</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Jenis Jabatan</label>
                            <div class="col-md-9">
                                <select name="jenisjab" class="form-control" required>
                                    <option value="">--Pilih Disini--</option>
                                    <option value="JFT">JFT</option>
                                    <option value="JFU">JFU</option>
                                    <option value="STRUKTURAL">STRUKTURAL</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="submit" name="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->