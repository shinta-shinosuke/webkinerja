<div class="container-fluid">
                <div class="panel panel-default">
                    <div class="panel-heading">Setting Tanggal Libur</div>
                    <div class="panel-body">
                        <a class="btn btn-sm btn-success" href="<?php echo site_url('opd/addsetting');?>"><i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;Tambah Data</a>
                        <hr/>
                        <table class="table table-bordered table-hover" id="mytable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Aksi</th>
                                    <th>Tanggal</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        <script src="<?php echo base_url();?>Admin/plugins/jQuery/jQuery-2.2.0.min.js"></script>
        <script src="<?php echo base_url('Admin/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('Admin/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">

            $(document).ready(function () {
                $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                var t = $('#mytable').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "searching" : false,
                    "ajax": '<?php echo site_url('opd/ajax_libur'); ?>',
                    "columns": [
                        {
                            "data": null,
                            "class": "text-center",
                            "orderable": false
                        },
                            {
                            "class": "text-center",
                            "data": "aksi"
                        },
                        {"data": "tanggal"},
                        {"data": "keterangan"},
                    ],
                    "order": [[1, 'desc']],
                    "rowCallback": function (row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
            });

function add_libur()
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Tambah Hari Libur'); // Set Title to Bootstrap modal title
}

function edit_pns(idlibur)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Edit Hari Libur'); // Set Title to Bootstrap modal title
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('opd/ajax_editlibur'); ?>/"+idlibur,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.idlibur);
            $('[name="tahun"]').val(data.tahun);
            $('[name="bulan"]').val(data.bulan);
            $('[name="tanggal"]').val(data.tanggal);
            $('[name="keterangan"]').val(data.keterangan);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

</script>
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <div class="modal-body form">
                <form action="<?php echo site_url('opd/ajax_addpns');?>" id="form" name="form" class="form-horizontal" role="form" method="POST">
                    <input type="hidden" name="id"/>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Bulan</label>
                            <div class="col-md-9">
                                <select name="bulan" class="form-control" required>
                                        <option value="">-- Pilih Tahun --</option>
                                        <option value="01">Januari</option>
                                        <option value="02">Februari</option>
                                        <option value="03">Maret</option>
                                        <option value="04">April</option>
                                        <option value="05">Mei</option>
                                        <option value="06">Juni</option>
                                        <option value="07">Juli</option>
                                        <option value="08">Agustus</option>
                                        <option value="09">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">Nopember</option>
                                        <option value="12">Desember</option>
                                    </select>
                                </div>
                                <label class="control-label col-md-3">Tahun</label>
                                <div class="col-md-9">
                                    <select name="tahun" class="form-control" required>
                                        <option value="">-- Pilih Tahun --</option>
                                        <?php
                                            $thn_skr = date('Y');
                                            for ($x = $thn_skr; $x >= 2010; $x--) {
                                        ?>
                                        <option value="<?php echo $x;?>" <?php if($x==$tahun){ echo " selected"; } ?>><?php echo $x; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tanggal</label>
                                <div class="col-md-9">
                                    <input type="text" name="tanggal" placeholder="Tanggal" class="form-control" required />
                                    <span class="help-block">Harap isi bidang ini dengan angka</span>
                            </div>
                            </div>
                            <div class="form-group" style="margin-bottom:0">
                                <div class="col-sm-offset-2 col-sm-10 text-right">
                                    <button type="submit" id="submit" name="submit" class="btn btn-primary">Simpan</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>