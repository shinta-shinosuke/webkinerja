<?php
$url = $this->uri->segment('2');
if($url=='editatasan')
{
    $tag = 'readonly';
}else{
    $tag = 'required';
}
?>
            <div class="container-fluid">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo $title; ?></div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="<?php echo base_url()?>ats/saveacc" method="post">
                            <div class="form-group">
                                <input type="hidden" name="id" class="form-control" value="<?php echo $id; ?>" />
                                <label for="inputEmail3" class="col-sm-2 control-label">NIP Atasan</label>
                                <div class="col-sm-6">
                                    <select name="nipatasan" class="form-control" readonly>
                                            <option value="">-- Pilih Disini --</option>
                                            <?php
                                                foreach ($pns->result_array() as $key => $v) {
                                            ?>
                                            <option value="<?php echo $v['nip'];?>" <?php if($v['nip']==$nipatasan){ echo " selected"; } ?>><?php echo $v['nip']." - ".$v['nama']; ?></option>
                                            <?php
                                                }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">NIP Staff</label>
                                <div class="col-sm-6">
                                    <select name="nip" class="form-control" required>
                                            <option value="">-- Pilih Disini --</option>
                                            <?php
                                                foreach ($nipstaff->result_array() as $key => $w) {
                                            ?>
                                            <option value="<?php echo $w['nip'];?>" <?php if($w['nip']==$nip){ echo " selected"; } ?>><?php echo $w['nip']." - ".$w['nama']; ?></option>
                                            <?php
                                                }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">BLTH</label>
                                <div class="col-sm-2">
                                    <input type="text" name="blth" class="form-control" id="datetimepicker2" value="<?php echo $blth; ?>" required />
                                    <span style="color:red"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-2">
                                    <input type="text" name="status" value="<?php echo $status; ?>" class="form-control" required />
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Persetujuan</label>
                                <div class="col-sm-3">
                                    <input type="text" name="tanggal" class="form-control" id="datetimepicker1" value="<?php echo $tanggal; ?>" required />
                                    <span style="color:red"></span>
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Jumlah Kegiatan</label>
                                <div class="col-sm-2">
                                    <input type="text" name="jml" value="<?php echo $jml; ?>" class="form-control" required />
                                    <span class="help-block"></span>
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Sumber Data</label>
                                <div class="col-sm-2">
                                    <input type="text" name="sumber" value="<?php echo $sumber; ?>" class="form-control" required />
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group text-right" style="margin-top:20px">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?php echo base_url();?>opd/acc"><button type="button" class="btn btn-warning">Batal</button></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>