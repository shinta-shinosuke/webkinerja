        <div id="cm-menu">
            <nav class="cm-navbar cm-navbar-primary">
                <div class="cm-flex"><a href="<?php echo base_url();?>" class="cm-logo"></a></div>
                <div class="btn btn-primary md-menu-white" data-toggle="cm-menu"></div>
            </nav>
            <div id="cm-menu-content">
                <div id="cm-menu-items-wrapper">
                    <div id="cm-menu-scroller">
                        <ul class="cm-menu-items">
                            <li><a href="<?php echo base_url();?>ats" class="sf-dashboard">Dashboard</a></li>
                            <li><a href="#" class="sf-notepad">Persetujuan Kinerja</a></li>
                            <li><a href="<?php echo base_url();?>ats/note" class="sf-window-layout">Penilaian Perilaku</a></li>
                            <li class="cm-submenu">
                                <a class="sf-cat">Data Staf<span class="caret"></span></a>
                                <ul>
                                    <li><a href="#">Sunting</a></li>
                                    <li><a href="#">Cetak</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>