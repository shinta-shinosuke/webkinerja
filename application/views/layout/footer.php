<?php
    if($this->session->userdata('userkodeadmin')==1)
    {
        $level = 'Admin BKD';
    }else if($this->session->userdata('userkodeadmin')==2)
    {
        $level = 'Admin OPD';
    }else if($this->session->userdata('userkodeadmin')==3 || $this->session->userdata('userkodeadmin')==4)
    {
        $level = 'PNS';
    }else{
        $level = 'Tamu';
    }
?>
<footer class="cm-footer">
	<span class="pull-left">
	Sign In as <?php echo $level;?>
	</span>
	<span class="pull-right">&copy; 2018 BKD Kab. Batang</span></footer>