        <div id="cm-menu">
            <nav class="cm-navbar cm-navbar-primary">
                <div class="cm-flex"><a href="<?php echo base_url();?>" class="cm-logo"></a></div>
                <div class="btn btn-primary md-menu-white" data-toggle="cm-menu"></div>
            </nav>
            <div id="cm-menu-content">
                <div id="cm-menu-items-wrapper">
                    <div id="cm-menu-scroller">
                        <ul class="cm-menu-items">
                            <li><a href="<?php echo base_url();?>" class="sf-dashboard">Dashboard</a></li>

                            <!-- ======================================================================================================= -->
                            <?php if($this->session->userdata('userkodeadmin')==1){
                            echo '<li><a href="'.base_url().'dashboard/atasan" class="sf-profile-group">Profil PNS</a></li>';
                            }?>

                            <!-- ======================================================================================================= -->
                            <?php if($this->session->userdata('userkodeadmin')==2 || $this->session->userdata('userkodeadmin')==1){ ?>
                            <li><a href="<?php echo base_url();?>opd/setting" class="sf-calendar">Jadwal Kerja</a></li>
                            <?php } ?>

                            <!-- ======================================================================================================= -->
                            <?php if($this->session->userdata('userkodeadmin')==2){ ?>
                            <li><a href="<?php echo base_url();?>opd/skp" class="sf-book-bookmark">SKP Tahunan</a></li>
                            <?php }else if($this->session->userdata('userkodeadmin')==1){ ?>
                            <li><a href="<?php echo base_url();?>dashboard/skp" class="sf-book-bookmark">SKP Tahunan</a></li>
                            <?php }else if($this->session->userdata('userkodeadmin')==4 || $this->session->userdata('userkodeadmin')==3){ ?>
                            <li><a href="<?php echo base_url();?>pns/pnsskp" class="sf-book-bookmark">SKP Tahunan</a></li>
                            <?php } ?>

                            <!-- ======================================================================================================= -->
                            <?php if($this->session->userdata('userkodeadmin')==2){ ?>
                            <li><a href="<?php echo base_url();?>opd/kskp" class="sf-file-bookmark">Laporan Kinerja SKP</a></li>
                            <?php }else if($this->session->userdata('userkodeadmin')==1){ ?>
                            <li><a href="<?php echo base_url();?>dashboard/kskp" class="sf-file-bookmark">Laporan Kinerja SKP</a></li>
                            <?php }else if($this->session->userdata('userkodeadmin')==4 || $this->session->userdata('userkodeadmin')==3){ ?>
                            <li><a href="<?php echo base_url();?>pns/pnskskp" class="sf-file-bookmark">Laporan Kinerja SKP</a></li>
                            <?php } ?>
                            
                            <!-- ======================================================================================================= -->
                            <?php if($this->session->userdata('userkodeadmin')==2){ ?>
                            <li><a href="<?php echo base_url();?>opd/kprod" class="sf-clock">Laporan Kinerja Produktivitas</a></li>
                            <?php }else if($this->session->userdata('userkodeadmin')==1){ ?>
                            <li><a href="<?php echo base_url();?>dashboard/kprod" class="sf-clock">Laporan Kinerja Produktivitas</a></li>
                            <?php }else if($this->session->userdata('userkodeadmin')==4 || $this->session->userdata('userkodeadmin')==3){ ?>
                            <li><a href="<?php echo base_url();?>pns/pnskprod" class="sf-clock">Laporan Kinerja Produktivitas</a></li>
                            <?php } ?>

                            <!-- ======================================================================================================= -->
                            <?php if($this->session->userdata('userkodeadmin')==2){ ?>
                            <li><a href="<?php echo base_url();?>opd/acc" class="sf-light-bulb">Persetujuan Atasan</a></li>
                            <?php }else if($this->session->userdata('userkodeadmin')==1){ ?>
                            <li><a href="<?php echo base_url();?>dashboard/acc" class="sf-light-bulb">Persetujuan Atasan</a></li>
                            <?php }else if($this->session->userdata('userkodeadmin')==3 || $this->session->userdata('userkodeadmin')==4){ ?>
                            <li><a href="<?php echo base_url();?>ats/acc" class="sf-light-bulb">Persetujuan Atasan</a></li>
                            <?php } ?>

                            <!-- ======================================================================================================= -->
                            <?php if($this->session->userdata('userkodeadmin')==2){ ?>
                            <li><a href="<?php echo base_url();?>opd/note" class="sf-monitor">Penilaian Perilaku</a></li>
                            <?php }else if($this->session->userdata('userkodeadmin')==1){ ?>
                            <li><a href="<?php echo base_url();?>dashboard/note" class="sf-monitor">Penilaian Perilaku</a></li>
                            <?php }else if($this->session->userdata('userkodeadmin')==4){ ?>
                            <li><a href="<?php echo base_url();?>pns/pnsnote" class="sf-monitor">Penilaian Perilaku</a></li>
                            <?php }else if($this->session->userdata('userkodeadmin')==3){ ?>
                            <li><a href="<?php echo base_url();?>ats/note" class="sf-monitor">Penilaian Perilaku</a></li>
                            <?php } ?>

                            <!-- ======================================================================================================= -->
                            <?php if($this->session->userdata('userkodeadmin')=='1'){
                                echo '<li><a href="#" class="sf-box-out">Ekspor Data Presensi</a></li>';
                            }?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>