            <div class="container-fluid">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo $title; ?></div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="<?php echo base_url()?>opd/saveskp" method="post">
                            <div class="form-group">
                                <input type="hidden" name="id" class="form-control" value="<?php echo $id; ?>" />
                                <input type="hidden" name="url" class="form-control" value="<?php echo $this->uri->segment(1); ?>" />
                                <label for="inputEmail3" class="col-sm-2 control-label">NIP</label>
                                <div class="col-sm-6">
                                    <select name="nip" class="form-control" required>
                                            <option value="">-- Pilih Disini --</option>
                                            <?php
                                                foreach ($pns->result_array() as $key => $v) {
                                            ?>
                                            <option value="<?php echo $v['nip'];?>" <?php if($v['nip']==$nip){ echo " selected"; } ?>><?php echo $v['nip']." - ".$v['nama']; ?></option>
                                            <?php
                                                }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Tahun</label>
                                <div class="col-sm-2">
                                    <select name="tahun" class="form-control" required>
                                        <option value="">-- Pilih Disini --</option>
                                        <?php
                                            $thn_skr = date('Y');
                                            for ($x = $thn_skr; $x >= 2010; $x--) {
                                        ?>
                                        <option value="<?php echo $x;?>" <?php if($x==$tahun){ echo " selected"; } ?>><?php echo $x; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Kegiatan</label>
                                <div class="col-sm-6">
                                <!--<textarea name="kegiatan" id="summernote"><?php echo $kegiatan; ?></textarea>-->
                                <textarea name="kegiatan" class="form-control"><?php echo $kegiatan; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Target Kuantitas</label>
                                <div class="col-sm-1">
                                    <input type="text" name="targetkuantitas" class="form-control" value="<?php echo $targetkuantitas; ?>" />
                                </div>
                                <div class="col-sm-5">
                                    <select name="satuankuantitas" class="form-control" required>
                                        <option value="">-- Pilih Disini --</option>
                                        <?php
                                            foreach ($kuantitas->result_array() as $key => $s) {
                                        ?>
                                            <option value="<?php echo $s['id'];?>" <?php if($s['id']==$satuankuantitas){ echo " selected"; } ?>><?php echo $s['kuantitas']; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Target Penyelesaian</label>
                                <div class="col-sm-1">
                                    <input type="text" name="targetpenyelesaian" class="form-control" value="<?php echo $targetpenyelesaian; ?>" />
                                </div>
                                <div class="col-sm-2">
                                    <input type="hidden"class="form-control" />Bulan
                                </div>
                            </div>
                            <div class="form-group text-right" style="margin-top:20px">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?php echo base_url();?>opd/skp"><button type="button" class="btn btn-warning">Batal</button></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>