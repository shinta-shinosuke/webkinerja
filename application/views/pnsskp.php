<div class="container-fluid">
                <div class="panel panel-default">
                    <div class="panel-heading">Data SKP Tahunan</div>
                    <div class="panel-body">
                        <!--<a class="btn btn-sm btn-success" href="javascript:void(0)" onclick="add_skp()"><i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;Tambah Data</a>-->
                        <a class="btn btn-sm btn-success" href="<?php echo base_url();?>pns/addskp"><i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;Tambah Data</a>
                        <hr/>
                        <table class="table table-bordered table-hover" id="mytable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Aksi</th>
                                    <th>Tahun</th>
                                    <th>NIP</th>
                                    <th>Nama</th>
                                    <th>Kegiatan Tahunan</th>
                                    <th>Target Kuantitas</th>
                                    <th>Satuan Kuantitas</th>
                                    <th>Target Selesai</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        <script src="<?php echo base_url();?>Admin/plugins/jQuery/jQuery-2.2.0.min.js"></script>
        <script src="<?php echo base_url('Admin/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('Admin/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">

            $(document).ready(function () {
                $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                var t = $('#mytable').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "searching" : false,
                    "ajax": '<?php echo site_url('pns/ajax_skp'); ?>',
                    "columns": [
                        {
                            "data": null,
                            "class": "text-center",
                            "orderable": false
                        },
                        {
                            "class": "text-center",
                            "data": "aksi"
                        },
                        {"data": "tahun"},
                        {"data": "nip"},
                        {"data": "nama"},
                        {"data": "kegiatan"},
                        {"data": "t_kuantitas"},
                        {"data": "s_kuantitas"},
                        {"data": "t_selesai"},
                    ],
                    "order": [[1, 'desc']],
                    "rowCallback": function (row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
            });
        </script>