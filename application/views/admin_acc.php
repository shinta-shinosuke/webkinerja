<div class="container-fluid">
                <div class="panel panel-default">
                    <div class="panel-heading">Data Persetujuan Atasan</div>
                    <div class="panel-body">
                        <table class="table table-bordered table-hover" id="mytable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Aksi</th>
                                    <th>NIP</th>
                                    <th>NIP Atasan</th>
                                    <th>BLTH</th>
                                    <th>Status</th>
                                    <th>Tgl Persetujuan</th>
                                    <th>Jml Kegiatan</th>
                                    <th>Sumber Data</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        <script src="<?php echo base_url();?>Admin/plugins/jQuery/jQuery-2.2.0.min.js"></script>
        <script src="<?php echo base_url('Admin/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('Admin/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">

            $(document).ready(function () {
                $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                var t = $('#mytable').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "searching" : false,
                    "ajax": '<?php echo site_url('dashboard/ajax_acc'); ?>',
                    "columns": [
                        {
                            "data": null,
                            "class": "text-center",
                            "orderable": false
                        },
                            {
                            "class": "text-center",
                            "data": "aksi"
                        },
                        {"data": "nip"},
                        {"data": "nip_atasan"},
                        {"data": "blth"},
                        {"data": "status"},
                        {"data": "tgl_persetujuan"},
                        {"data": "jml_kegiatan"},
                        {"data": "sumber_data"},
                    
                    ],
                    "order": [[1, 'desc']],
                    "rowCallback": function (row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
            });
        </script>
