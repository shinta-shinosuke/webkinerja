            <div class="container-fluid">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo $title; ?></div>
                    <div class="panel-body">
                        <span style="color:red"><b><?php echo $error;?></b></span>
                        <br/>
                        <br/>
                        <form class="form-horizontal" action="<?php echo base_url()?>opd/saveakun" method="post">
                            <div class="form-group">
                                <input type="hidden" name="nip" class="form-control" value="<?php echo $nip; ?>" />
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Password Lama</label>
                                    <div class="col-sm-6">
                                        <input type="password" name="password" value="<?php echo $password; ?>" class="form-control" required />
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Password Baru</label>
                                    <div class="col-sm-6">
                                        <input type="password" name="passwordbaru" value="<?php echo $passwordbaru; ?>" class="form-control" required />
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Konfirmasi Password Baru</label>
                                    <div class="col-sm-6">
                                        <input type="password" name="repasswordbaru" value="<?php echo $repasswordbaru; ?>" class="form-control" required />
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-right" style="margin-top:20px">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?php echo base_url();?>welcome"><button type="button" class="btn btn-warning">Batal</button></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>