<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
        <link rel="icon" type="image/png" href="<?php echo base_url();?>Admin/images/icons/favicon.png"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>Admin/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>Admin/css/bootstrap-clearmin.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>Admin/css/roboto.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>Admin/css/material-design.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>Admin/css/small-n-flat.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>Admin/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>Admin/css/summernote.css">
        <link href="<?php echo base_url('Admin/css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url();?>Admin/datatables/dataTables.min.css"/>
        <link rel = "stylesheet" href = "<?php echo base_url();?>Admin/css/jquery-ui.css">
        <title>e-Kinerja</title>
    </head>
    <body class="cm-no-transition cm-2-navbar">
                <?php echo $sidebar; ?>    
        <header id="cm-header">
            <?php echo $header; ?>
            <!--<nav class="cm-navbar cm-navbar-default cm-navbar-slideup">
                <div class="cm-flex">
                    <div class="nav-tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="dashboard-sales.html">Sales analytics</a></li>
                            <li><a href="dashboard-visitors.html">Website Visitors</a></li>
                            <li><a href="dashboard-server_stats.html">Server Stats</a></li>
                        </ul>
                    </div>
                </div>
                <div class="pull-right" style="border-left:1px solid #e5e5e5"><a title="Download as PDF" class="btn btn-default btn-light md-file-download"></a></div>
                <div class="pull-right"><a title="Customize indicators" class="btn btn-default btn-light md-settings"></a></div>
            </nav>-->
        </header>
        <div id="global" style="padding-top:52px">
            <?php echo $contents; ?>
            <?php echo $footer; ?>
        </div>
        <script src="<?php echo base_url();?>Admin/js/lib/jquery-2.1.3.min.js"></script>
        <script type = "text/javascript" src="<?php echo base_url();?>Admin/js/jquery-ui.min.js"></script>
          <script>
             $(function() {
                $( "#tanggal" ).datepicker({
                    autoclose: true,
                    dateFormat: "dd-mm-yy"
                });
                $('#mulai').datepicker({
                  format: "mm-yyyy",
                  viewMode: "months", 
                  minViewMode: "months",
                });

                $('#sampai').datepicker({
                  format: "mm-yyyy",
                  viewMode: "months", 
                  minViewMode: "months",
                });
             });
          </script>
        <script src="<?php echo base_url();?>Admin/js/jquery.mousewheel.min.js"></script>
        <script src="<?php echo base_url();?>Admin/js/jquery.cookie.min.js"></script>
        <script src="<?php echo base_url();?>Admin/js/fastclick.min.js"></script>
        <script src="<?php echo base_url();?>Admin/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>Admin/js/clearmin.min.js"></script>
        <script src="<?php echo base_url();?>Admin/js/summernote.min.js"></script>
        <script src="<?php echo base_url();?>Admin/js/demo/notepad.js"></script>
        <script src="<?php echo base_url();?>Admin/js/moment.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('Admin/js/bootstrap-datetimepicker.min.js') ?>"></script>
        <script src="<?php echo base_url();?>Admin/datatables/jquery.dataTables.js"></script>

<script type="text/javascript">
        /* Data Table */
            $(function() {
                $("#example1").DataTable();
                $('#example2').DataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": true
                });
            });
</script>
<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
                   format: 'DD-MM-YYYY HH:mm:ss'
                });
                $('#datetimepicker2').datetimepicker({
                   format: 'MM-YYYY'
                });
            });
        </script>
        <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script type="text/javascript">
                $(document).ready(function() {
                    $('select[name="nip"]').on('change', function() {
                        var stateID = $(this).val();
                        if(stateID) {
                            $.ajax({
                                url: "<?php echo site_url('opd/myformAjax'); ?>/"+stateID,
                                type: "GET",
                                dataType: "json",
                                success:function(data) {
                                    $('select[name="skp"]').empty();
                                    $("<option value='' selected='selected'>- Pilih Disini -</option>").prependTo("#skp");
                                    $.each(data, function(key, value) {
                                        $('select[name="skp"]').append('<option value="'+ value.id_skp +'">'+ value.skptahunan +'</option>');
                                        $('#kuantitas').val('0');
                                    });
                                }
                            });
                        }else{
                            $('select[name="skp"]').empty();
                        }
                    });

                        $('select[name="skp"]').on('change', function() {
                            var idskp = $(this).val();
                            if(idskp) {
                                $.ajax({
                                    url: "<?php echo site_url('opd/myeditformAjax'); ?>/"+idskp,
                                    type: "GET",
                                    dataType: "json",
                                    success:function(data) {
                                        $.each(data, function(key, value) {
                                            $('#kuantitas').val('0');
                                            $('#satuan').val(value.satuan);
                                            $('#numsatuan').val(value.numsatuan);
                                        });
                                    }
                                });
                            }else{
                                $('#satuan').empty();
                            }
                        });
                });
            </script>
    </body>
</html>