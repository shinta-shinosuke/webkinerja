            <div class="container-fluid">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo $title; ?></div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="<?php echo base_url()?>ats/savenote" method="post">
                            <input type="hidden" name="id" class="form-control" value="<?php echo $id; ?>" />
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">NIP</label>
                                <div class="col-sm-6">
                                    <select name="nipstaf" id="nipstaf" class="form-control" required >
                                            <option value="">-- Pilih Disini --</option>
                                            <?php
                                                foreach ($pns->result_array() as $key => $v) {
                                            ?>
                                            <option value="<?php echo $v['nip'];?>" <?php if($v['nip']==$nip){ echo " selected"; } ?>><?php echo $v['nip']." - ".$v['nama']; ?></option>
                                            <?php
                                                }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Pelayanan</label>
                                <div class="col-sm-2">
                                    <input type="text" name="pelayanan" id="pelayanan" class="form-control" value="<?php echo $pelayanan; ?>"  required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Integritas</label>
                                <div class="col-sm-2">
                                    <input type="text" name="integritas" id="integritas" class="form-control" value="<?php echo $pelayanan; ?>"  required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Komitmen</label>
                                <div class="col-sm-2">
                                    <input type="text" name="komitmen" id="komitmen" class="form-control" value="<?php echo $komitmen; ?>"  required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Disiplin</label>
                                <div class="col-sm-2">
                                    <input type="text" name="disiplin" id="disiplin" class="form-control" value="<?php echo $disiplin; ?>"  required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Kerjasama</label>
                                <div class="col-sm-2">
                                    <input type="text" name="kerjasama" id="kerjasama" class="form-control" value="<?php echo $kerjasama; ?>"  required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Kepemimpinan</label>
                                <div class="col-sm-2">
                                    <input type="text" name="kepemimpinan" id="kepemimpinan" class="form-control" value="<?php echo $kepemimpinan; ?>"  required />
                                </div>
                            </div>
                            <div class="form-group text-right" style="margin-top:20px">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?php echo base_url();?>ats/note"><button type="button" class="btn btn-warning">Batal</button></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            