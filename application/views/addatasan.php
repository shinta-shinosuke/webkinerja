<?php
$url = $this->uri->segment('2');
if($url=='editatasan')
{
    $tag = 'readonly';
}else{
    $tag = 'required';
}
?>
            <div class="container-fluid">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo $title; ?></div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="<?php echo base_url()?>dashboard/saveatasan" method="post">
                            <div class="form-group">
                                <input type="hidden" name="id" class="form-control" value="<?php echo $id; ?>" />
                                <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Hak Akses</label>
                                <div class="col-sm-6">
                                    <select name="kodeadmin" class="form-control" readonly />
                                            <option value="">-- Pilih Disini --</option>
                                            <option value="4" <?php if($kodeadmin==4){ echo " selected"; } ?>>User</option>
                                            <option value="2" <?php if($kodeadmin==2){ echo " selected"; } ?>>Admin OPD</option>
                                            <option value="1" <?php if($kodeadmin==1){ echo " selected"; } ?>>Admin BKD</option>
                                    </select>
                                </div>
                            </div>
                                <label for="inputEmail3" class="col-sm-2 control-label">User ID/NIP</label>
                                <div class="col-sm-6">
                                    <select name="nip" class="form-control" <?php echo $tag; ?> readonly />
                                            <option value="">-- Pilih Disini --</option>
                                            <?php
                                                foreach ($nipstaff->result_array() as $key => $v) {
                                            ?>
                                            <option value="<?php echo $v['nip'];?>" <?php if($v['nip']==$nip){ echo " selected"; } ?>><?php echo $v['nip']." - ".$v['nama']; ?></option>
                                            <?php
                                                }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-6">
                                    <input type="text" name="password" value="<?php echo $password; ?>" class="form-control" />
                                    <span class="help-block">* abaikan jika tidak ingin merubah password</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Atasan Langsung</label>
                                <div class="col-sm-6">
                                    <select name="nip1" class="form-control" required />
                                            <option value="">-- Pilih Disini --</option>
                                            <?php
                                                foreach ($pns->result_array() as $key => $w) {
                                            ?>
                                            <option value="<?php echo $w['nip'];?>" <?php if($w['nip']==$nip1){ echo " selected"; } ?>><?php echo $w['nip']." - ".$w['nama']; ?></option>
                                            <?php
                                                }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Atasan</label>
                                <div class="col-sm-6">
                                    <select name="nip2" class="form-control" required />
                                            <option value="">-- Pilih Disini --</option>
                                            <?php
                                                foreach ($pns->result_array() as $key => $x) {
                                            ?>
                                            <option value="<?php echo $x['nip'];?>" <?php if($x['nip']==$nip2){ echo " selected"; } ?>><?php echo $x['nip']." - ".$x['nama']; ?></option>
                                            <?php
                                                }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Atasan</label>
                                <div class="col-sm-6">
                                    <select name="nip3" class="form-control">
                                            <option value="">-- Pilih Disini --</option>
                                            <?php
                                                foreach ($pns->result_array() as $key => $y) {
                                            ?>
                                            <option value="<?php echo $y['nip'];?>" <?php if($y['nip']==$nip3){ echo " selected"; } ?>><?php echo $y['nip']." - ".$y['nama']; ?></option>
                                            <?php
                                                }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Unit Kerja</label>
                                <div class="col-sm-6">
                                    <select name="unit" class="form-control" readonly />
                                            <option value="">-- Pilih Disini --</option>
                                            <?php
                                                foreach ($unit->result_array() as $key => $z) {
                                            ?>
                                            <option value="<?php echo $z['kd'];?>" <?php if($z['kd']==$loker){ echo " selected"; } ?>><?php echo $z['nama']; ?></option>
                                            <?php
                                                }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Jabatan</label>
                                <div class="col-sm-6">
                                    <input type="text" name="jabatan" value="<?php echo $jabatan; ?>" class="form-control" readonly />
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group text-right" style="margin-top:20px">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?php echo base_url();?>dashboard/atasan"><button type="button" class="btn btn-warning">Batal</button></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>  