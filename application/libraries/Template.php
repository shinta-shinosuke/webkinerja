<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template {
		var $template_data = array();
		
		function set($name, $value)
		{
			$this->template_data[$name] = $value;
		}
	
		function load($template = '', $view = '' , $view_data = array(), $return = FALSE)
		{               
			$this->CI =& get_instance();
			$this->set('header', $this->CI->load->view('layout/header', '', TRUE));			
			$this->set('contents', $this->CI->load->view($view, $view_data, TRUE));			
			$this->set('sidebar', $this->CI->load->view('layout/opd-sidebar', '', TRUE));	
			$this->set('footer', $this->CI->load->view('layout/footer', '', TRUE));	
			return $this->CI->load->view($template, $this->template_data, $return);
		}

		function admin($template = '', $view = '' , $view_data = array(), $return = FALSE)
		{               
			$this->CI =& get_instance();
			$this->set('header', $this->CI->load->view('layout/header', '', TRUE));			
			$this->set('contents', $this->CI->load->view($view, $view_data, TRUE));			
			$this->set('sidebar', $this->CI->load->view('layout/admin-sidebar', '', TRUE));	
			$this->set('footer', $this->CI->load->view('layout/footer', '', TRUE));	
			return $this->CI->load->view($template, $this->template_data, $return);
		}

		function opd($template = '', $view = '' , $view_data = array(), $return = FALSE)
		{               
			$this->CI =& get_instance();
			$this->set('header', $this->CI->load->view('layout/header', '', TRUE));			
			$this->set('contents', $this->CI->load->view($view, $view_data, TRUE));			
			$this->set('sidebar', $this->CI->load->view('layout/opd-sidebar', '', TRUE));	
			$this->set('footer', $this->CI->load->view('layout/footer', '', TRUE));	
			return $this->CI->load->view($template, $this->template_data, $return);
		}

		function pns($template = '', $view = '' , $view_data = array(), $return = FALSE)
		{               
			$this->CI =& get_instance();
			$this->set('header', $this->CI->load->view('layout/header', '', TRUE));			
			$this->set('contents', $this->CI->load->view($view, $view_data, TRUE));			
			$this->set('sidebar', $this->CI->load->view('layout/opd-sidebar', '', TRUE));	
			$this->set('footer', $this->CI->load->view('layout/footer', '', TRUE));	
			return $this->CI->load->view($template, $this->template_data, $return);
		}

		function ats($template = '', $view = '' , $view_data = array(), $return = FALSE)
		{               
			$this->CI =& get_instance();
			$this->set('header', $this->CI->load->view('layout/header', '', TRUE));			
			$this->set('contents', $this->CI->load->view($view, $view_data, TRUE));			
			$this->set('sidebar', $this->CI->load->view('layout/ats-sidebar', '', TRUE));	
			$this->set('footer', $this->CI->load->view('layout/footer', '', TRUE));	
			return $this->CI->load->view($template, $this->template_data, $return);
		}

		function visitor($template = '', $view = '' , $view_data = array(), $return = FALSE)
		{               
			$this->CI =& get_instance();
			$this->set('header', $this->CI->load->view('layout/header', '', TRUE));			
			$this->set('contents', $this->CI->load->view($view, $view_data, TRUE));			
			$this->set('sidebar', $this->CI->load->view('layout/visitor-sidebar', '', TRUE));	
			$this->set('footer', $this->CI->load->view('layout/footer', '', TRUE));	
			return $this->CI->load->view($template, $this->template_data, $return);
		}

}

/* End of file Template.php */
/* Location: ./system/application/libraries/Template.php */