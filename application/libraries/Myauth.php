<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Myauth{

	
	function __construct(){
		$this->ci =& get_instance();
	}

	public function set_database($db_name)
    {
		$db_data = $this->ci->load->database($db_name, TRUE);
		$this->ci->db = $db_data;
	}

	public function login($cond=array(),$table){
	/* $cond = username & password */
	
		$data = array();
		foreach ($cond as $key => $value) {
			$data[$key] = $value;
		}
		
		$query = $this->ci->db->get_where($table,$data);

		if($query->num_rows() != 1){

			return false;
		}else{
			foreach ($query->result_array() as $key => $v) 
			{ 
				$nip   = $v['nip']; 
				$nama  = $v['nama']; 
				$opd   = $v['opd'];
				$subunitkerja = $v['subunitkerja'];
				$kode_admin = $v['kode_admin'];
			}
			$cek = $this->ci->db->query("SELECT kode_admin, nip_atasan1, nip_atasan2, nip_atasan3 FROM vlogin WHERE nip_atasan1='".$nip."' OR nip_atasan2='".$nip."' OR nip_atasan3='".$nip."' AND kode_admin=4");
			if($cek->num_rows()>0)
			{
				$kodeadmin=3;
			}else{
				$kodeadmin=$kode_admin;
			}

			$data = array(
					'nip' => $nip,
					'nama' => $nama,
					'opd' => $opd,
					'subunitkerja' => $subunitkerja,
					'kode_admin' => $kodeadmin
				);

			return $data;
		}
	}
	
	function logged_in(){
	// mahasiswa atau empty level just stop at gerbang admin aja 
	
		$levelid = $this->ci->session->userdata('userkodeadmin');

		if($levelid == '')
		{
			header('location: '.site_url('login'));	
		}
		
	}

	function logged_in_admin(){
		$levelid = $this->ci->session->userdata('userkodeadmin');

		if($levelid == '1')
		{
			header('location: '.site_url('dashboard'));	
		}else{
			header('location: '.site_url('welcome'));	
		}
		
	}

	function logged_in_opd(){
	// mahasiswa atau empty level just stop at gerbang admin aja 
	
		$levelid = $this->ci->session->userdata('userkodeadmin');

		if($levelid == '2')
		{
			header('location: '.site_url('opd'));	
		}else{
			header('location: '.site_url('welcome'));	
		}
		
	}

	function logged_in_ats(){
	// mahasiswa atau empty level just stop at gerbang admin aja 
	
		$levelid = $this->ci->session->userdata('userkodeadmin');

		if($levelid == '3')
		{
			header('location: '.site_url('ats'));	
		}else{
			header('location: '.site_url('welcome'));	
		}
		
	}

	function logout($props=array(),$page){
	/* $page nya Mahasiswa ke beranda */
	/* $page nya Dosen ke admin/login */
	
		foreach($props as $key => $value){
			
			$this->ci->session->unset_userdata($value);
			
		}

		header('location: '.site_url($page));
	}
}