<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AKses_h extends CI_Controller {

	function __construct(){
        parent::__construct();
        $ci =& get_instance();

        $cek  = $ci->session->userdata('id_session');
        $blok = $ci->session->userdata('blokir');
        $lvl  = $ci->session->userdata('ketopr');

        if( $cek == 'superuser' || $cek == 'administrator' || $cek == 'kabag' || $cek == 'kasubag' || $cek == 'operator')
        {
            if($blok == '0'){
               echo 'welcome';
            }else{
                echo'<script>alert("Akun Anda Diblokir. Hubungi Administrator !");window.location.href = "'.base_url('login').'";</script>';    
            }
        }else{
            echo'<script>alert("Akses Ditolak. Hubungi Administrator !");window.location.href = "'.base_url('login').'";</script>';    
        }
    }
}
